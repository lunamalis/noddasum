# Lunamalis (Noddasum)

## What

This is an open-source space-related game.

## How do I play

See the [Installation guide](#Installation).

## Credits

See [CREDITS.md](CREDITS.md) for credit information.

## Installation

The easy way to do this is to simply copy the `dist` directory to your computer.
You can do this by downloading the repository as a zip file.

Once you have done this, install `node`. Try to use version `10.16.3`.
Newer versions and older versions may still work, but are less likely.
Newer versions are more likely to work than older versions.

Then, install `npm`, version `6.13.1`. Again, the version may be adaptable.

Run `npm install` once.

Run once `cp -r dist/. build/` to copy all files from `dist` to `build`.

Every time you want to run the game, run `./scripts/run.bash`.
Then, open your webbrowser and go to `http://localhost:8090/index.html`.

To install from scratch, you will need to [compile](#Compiling).

## Compiling

You will need the following software installed:

* `node==10.16.3`
* `npm==6.13.1`

You may be able to use other versions. Newer versions are more likely to work
than older versions.

You will need to run the following *one* time:

```bash
$ npm install --dev
...
$ ./scripts/copy-public.bash
...
$ ./scripts/build-server.bash
...
$ ./scripts/build-client.bash
...
```

Now, every time you want to play the game, start the server by running the
following:

```bash
$ ./scripts/run.bash
...
```

Then, open your webbrowser to `http://localhost:8090/index.html`.

## How to make mods

Mods are installed in the [shared directory (src/ts/shared/mods)](src/ts/shared/mods).

You can look at the vanilla `lunamalis` for basis on how to make your own.

You will have to recompile the server every time you create or edit a mod. The
`./scripts/build-server.bash` script does this. However, you will have to restart
your server (`./scripts/run.bash`) each time you rebuild, of course.

## TODO List

* Possibly move away from Webpack on the server-side.
  * Consider using something like `tsc --watch`.
