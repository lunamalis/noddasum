const path = require('path');
module.exports = {
	entry: './src/ts/client/client.ts',
	mode: 'development',
	target: 'web',

	module: {
		rules: [
			{
				test: /.tsx?$/,
				use: [{
					loader: 'ts-loader',
					options: {
						configFile: "tsconfig-client.json"
					}
				}],
				exclude: path.resolve(__dirname, "node_modules")
			}
		]
	},
	resolve: {
		extensions: ['.ts', '.js'],
		alias: {
			'_': path.resolve(__dirname, "src", "ts"),
			'_co': path.resolve(__dirname, "src", "ts", "shared", "config.ts"),
			'_c': path.resolve(__dirname, "src", "ts", "client"),
			'_sh': path.resolve(__dirname, "src", "ts", "shared"),
			'_s': path.resolve(__dirname, "src", "ts", "server")
		}
	},
	// externals: [require('webpack-node-externals')()],
	externals: {
		"phaser-ce": "Phaser"
	},
	output: {
		filename: 'main.js',
		path: path.resolve(__dirname, 'build', 'client')
	}
};
