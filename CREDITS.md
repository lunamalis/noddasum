---
# Credits.md

* Used free spaceship sprites by MillionthVector.
* * License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
* * No changes were made.
* * [Source](http://web.archive.org/web/20191203200716/http://millionthvector.blogspot.com/p/free-sprites.html)
---
