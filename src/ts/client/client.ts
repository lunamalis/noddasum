import CONFIG from '_co';
import Client from '_c/client_class';

function main() {
	console.log("Welcome to version " + CONFIG.VERSION + ".");

	// @ts-ignore
	window.out = window.out ? window.out : {};
	out.main = main;
	const c = new Client();
	out.c = c;
	c.start();
}

window.addEventListener("DOMContentLoaded", main);
