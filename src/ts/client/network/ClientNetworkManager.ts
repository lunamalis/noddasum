import IProtocolsServer from "_/shared/interfaces/IProtocolsServer";
import IProtocolsClient from "_/shared/interfaces/IProtocolsClient";
import IProtocolsInit from "_/shared/interfaces/IProtocolsInit";

class ClientNetworkManager implements IProtocolsClient {

	"heartbeat"() {}
	"map.request"() {}
	"understand.shipTypes"() {}
	"understand.shipType"() {}
	"battle.init"() {}
	"battle.ready"() {}

	// /**
	//  * This is a dictionary.
	//  * The keys refer to the names of events released by the server.
	//  * The values are functions that we create that are fired.
	//  */
	// private _receivers: IProtocolsServer
	// private _senders: IProtocolsClient

	// /**
	//  * Register something for this.
	//  *
	//  * Automatically calls `argument.registerClientNetwork()`
	//  */
	// register(obj: { registerClientNetwork: () => IProtocolsInit<IProtocolsServer, IProtocolsClient>; }) {
	// 	const clientData = obj.registerClientNetwork();
	// 	for (const [key, value] of Object.entries(clientData.receivers)) {
	// 		this._registerReceiver(clientData.name, key, value);
	// 	}
	// 	for (const [key, value] of Object.entries(clientData.senders)) {
	// 		this._registerSender(clientData.name, key, value);
	// 	}
	// }

	// receive(): void {
	// 	for (let i = 0; i < Object.values(this._receivers).length; i++) {
	// 		const receiver = this._receivers[i];
	// 		receiver();
	// 	}
	// }

	// private _registerReceiver(name: string, key: string, value: Function): void {
	// 	if (!this._receivers.hasOwnProperty(key)) {
	// 		throw new Error(`Invalid key for registering network receiver: ${name} ${key} ${value}`);
	// 	}
	// 	if (this._receivers[key].hasOwnProperty(name)) {
	// 		throw new Error(`Invalid name for registering network receiver. Name already exists: ${name} ${key} ${value}`);
	// 	}
	// 	this._receivers[key][name] = value;
	// }

	// private _registerSender(name: string, key: string, value: Function): void {
	// 	if (!this._senders.hasOwnProperty(key)) {
	// 		throw new Error(`Invalid key for registering network sender: ${name} ${key} ${value}`);
	// 	}
	// 	if (this._senders[key].hasOwnProperty(name)) {
	// 		throw new Error(`Invalid name for registering network sender. Name already exists: ${name} ${key} ${value}`);
	// 	}
	// 	this._senders[key][name] = value;
	// }

}

export = ClientNetworkManager
