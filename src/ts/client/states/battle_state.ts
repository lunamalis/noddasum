import Phaser from 'phaser-ce';
import Client from '_c/client_class';
import Ship from '../classes/battle/Ship';
import IPrBattleInit from '_/shared/interfaces/protocols/IPrBattleInit';
import BattleShip from '_/shared/interfaces/IBattleShip';
import IPrShipType from '_/shared/interfaces/protocols/IPrShipType';
import deepFreeze from '_/shared/util/deepFreeze';

export default class BattleState extends Phaser.State {
	static readonly NAME: string = "Battle"

	client: Client
	ships: Array<Ship>
	shipTypes: {[domain: string]: {[name: string]: IPrShipType}}
	private _playerShipId: number
	private _cursors: Phaser.CursorKeys
	private _showDebug: boolean = false
	private _keys: {[key: string]: Phaser.Key}
	private _collision: Phaser.Physics.P2.CollisionGroup
	private _group: Phaser.Group

	init() {
		this.ships = [];
		this.shipTypes = {};

		this.client.socket.on("replyShipTypes", this._replyShipTypes.bind(this));
		this.client.socket.on("replyBattleInit", this._onReplyBattleInit.bind(this));
		this.client.socket.on("battleCreate", this._battleCreate.bind(this));
	}

	preload() {
		this.game.load.image('faction5ship1', 'resources/assets/lunamalis/textures/F5S1.png');
		this.game.load.image('faction5ship2', 'resources/assets/lunamalis/textures/F5S2.png');
	}

	create() {

		this._keys = {
			debug: this.game.input.keyboard.addKey(Phaser.Keyboard.UNDERSCORE)
		};

		this.game.physics.startSystem(Phaser.Physics.P2JS);
		this.game.world.setBounds(-1000, -1000, 1000, 1000);
		this.game.physics.p2.setImpactEvents(true);
		this.game.physics.p2.restitution = 0.8;

		this._collision = this.game.physics.p2.createCollisionGroup();

		this.game.physics.p2.updateBoundsCollisionGroup();

		this._group = this.game.add.group();
		this._group.enableBody = true;
		this._group.physicsBodyType = Phaser.Physics.P2JS;

		this.game.time.desiredFps = 60;

		this._cursors = this.game.input.keyboard.createCursorKeys();

		this._playerShipId = 0;
		this.client.socket.emit("battleInit");

		// TODO:
		// Also consider using this instead of a `_create_post`:
		// (async () => await downloadShipOrSomething(shipData))();
	}

	update() {
		const p = this.ships[this._playerShipId];
		// eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
		if (p) {
			if (this._cursors.up.isDown) {
				p.sprite.body.thrust(200);
			}
			else if (this._cursors.down.isDown) {
				p.sprite.body.thrust(-200);
			}

			if (this._cursors.left.isDown) {
				p.sprite.body.angularForce -= 1;
			}
			else if (this._cursors.right.isDown) {
				p.sprite.body.angularForce += 1;
			}
		}

		if (this._keys.debug.justDown) {
			console.log(`Setting debug mode to ${!this._showDebug}`);
			this._showDebug = !this._showDebug;
			for (const ship of Object.values(this.ships)) {
				// this.game.debug.body(ship.sprite);
				ship.sprite.body.debug = this._showDebug;
			}
			if (!this._showDebug) { this.game.debug.reset(); }
		}
	}

	render() {
		if (this._showDebug) {
			// TODO: add debug
		}
	}

	private _create_post(): void {
		this.game.camera.follow(this.ships[this._playerShipId].sprite);
	}

	// private _add_ship(data: BattleShip) {
	// 	const st = this._get_ship_type_by_did(data.shipDomain, data.shipId);
	// 	const ship = new Ship(this, data.id, this._group, this._collision, st.name);
	// 	this.ships.push(ship);
	// }

	private _add_ship(ship) {
		const shipType = this._get_ship_type_by_did(ship.domain, ship.name);
		const s = new Ship(this, ship.id, this._group, this._collision, shipType);
		this.ships.push(s);
	}

	// did === domain and id
	private _get_ship_type_by_did(domain: string, id: string): IPrShipType {
		const d = this.shipTypes[domain];
		if (d === undefined) {
			throw new Error(`Undefined ship domain: ${domain}. Also requested id ${id}.`);
		}
		// See?
		const st = d[id];
		if (st === undefined) {
			throw new Error(`Undefined ship id: ${id}. Also requested domain ${domain}.`);
		}
		return st;
	}

	/**
	 * This is called when the server replies with the "replyBattleInit" message.
	 *
	 * It brings us the required ship types in the battle.
	 *
	 * @param battleInit
	 */
	private _onReplyBattleInit(battleInit: IPrBattleInit): void {
		const needed: IPrBattleInit = {};
		for (const [domain, ids] of Object.entries(battleInit)) {
			if (this.shipTypes[domain] === undefined) {
				this.shipTypes[domain] = {};
				needed[domain] = [];
				// If we don't have anything in this domain, just add all of the ids into our `needed` array.
				for (const id of ids) {
					needed[domain].push(id);
				}
			}
			for (const id of ids) {
				if (this.shipTypes[domain][id] === undefined) {
					needed[domain].push(id);
				}
			}
		}
		this.client.socket.emit("understandShipTypes", needed);
	}

	private _set_ship_type_by_did(domain: string, id: string, st: IPrShipType): void {
		if (!this.shipTypes[domain]) {
			this.shipTypes[domain] = {};
		}
		this.shipTypes[domain][id] = st;
	}

	private _replyShipTypes(data: { [domain: string]: { [id: string]: IPrShipType } }) {
		console.log("Ship types:", data);
		for (const [domain, ids] of Object.entries(data)) {
			if (this.shipTypes[domain] === undefined) { this.shipTypes[domain] = {}; }
			for (const [name, value] of Object.entries(ids)) {
				// Freeze!
				this.shipTypes[domain][name] = deepFreeze(value);
				// Also laod our assets.
				const textureName: string = `ship--${domain}-${name}`;
				const texturePath: string = `resources/assets/${domain}/textures/${value.texture}.png`
				console.log("Loading texture:", textureName, texturePath);
				this.game.load.image(textureName, texturePath);
			}
		}
		this.game.load.start();
		this.game.load.onLoadComplete.addOnce(() => {
			this.client.socket.emit("battleReady");
		});
	}

	private _battleCreate(data: Array<any>) {
		console.log("Adding ships:", data);
		for (const ship of data) {
			this._add_ship(ship);
		}

		this._create_post()
	}
}
