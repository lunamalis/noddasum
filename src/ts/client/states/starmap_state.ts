import Phaser, { Point } from 'phaser-ce';
import Star from '_c/classes/Star';
import Hyperlane from '_c/classes/Hyperlane';
import BattleState from './battle_state';
import Client from '../client_class';
import MapData from '_/shared/interfaces/protocols/IMapData';
import IProtocolsServer from '_sh/interfaces/IProtocolsServer';

export default class StarmapState extends Phaser.State {
	static readonly NAME = "Starmap";
	stars: Array<Star>

	starsById: {[key: number]: Star}
	hyperlaneGraphics: Array<Hyperlane>
	activeStar: number
	client: Client
	private _starIds: Array<number>

	init(): void {
		this.stars = [];
		this.starsById = {};
		this._starIds = [];
		this.hyperlaneGraphics = [];
	}

	// preload(): void {}

	create(): void {
		const goText = this.game.add.text(10, 10, 'Go', {
			font: '1em Courier',
			fill: '#ffffff',
			align: 'center'
		});
		goText.inputEnabled = true;
		goText.events.onInputDown.add(this.onClickGo.bind(this));

		// this._generateStars();
		// this.activeStar = this.getRandomStarId();
		// this._connectStars();
		this.client.socket.on('sendMap', this._onGetMap.bind(this));
		this.client.socket.emit('requestMap');
	}

	getRandomStarId(): number {
		return this._starIds[Math.floor(Math.random() * this._starIds.length)];
	}

	getRandomStar(): Star {
		return this.starsById[this.getRandomStarId()];
	}

	getTwoStars(): [Star, Star] {
		const a: Star = this.getRandomStar();
		let tries: number = 0;
		while (tries < 100) {
			const try_ = this.getRandomStar();
			if (try_.id !== a.id) {
				return [a, try_]
			}
			tries++;
		}
		throw new Error("Tried over 100 times to get two random stars.");
	}

	onClickGo() {
		this.game.state.start(BattleState.NAME);
	}

	private _onGetMap(message: MapData) {
		console.log(message);
		// The reason we use a `for-in` here is because we get an object that
		// looks vaguely like:
		// {1: data, 1234: data, 3999: data}
		// etc
		// eslint-disable-next-line no-restricted-syntax
		for (const starIndex in message.stars) {
			const star = message.stars[starIndex];
			console.log(star);
			this._buildStar(star);
		}
	}

	private _buildStar(star: { id: number; x: number; y: number }): Star {
		const s = new Star(this, star.id, new Phaser.Point(star.x, star.y));
		this.starsById[star.id] = s;
		this.stars.push(s);
		return s;
	}

	static getRandomXY(): Phaser.Point {
		return new Phaser.Point(
			Math.floor(Math.random() * 250),
			Math.floor(Math.random() * 250)
		)
	}
}
