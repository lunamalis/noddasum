import Phaser from 'phaser-ce';
import StarmapState from './starmap_state';
export = class TitleState extends Phaser.State {

	static NAME: string = "title"
	// Lunamalis - 9 letters, 9 slots
	static readonly titleTextColors: Array<string> = [
		'#333333',
		'#444444',
		'#333333',
		'#444444',
		'#555555',
		'#777777',
		'#888888',
		'#999999',
		'#aaaaaa'
	]
	static readonly DefaultTitleColor: string = '#eeeeee'
	static readonly titleSpeed: number = 50
	texts: ITitleTexts

	titleTextColorPosition: number
	titleTimeout: ReturnType<typeof setTimeout>

	init() {
		this.texts = {
			title: null,
			play: null
		}
		this.titleTextColorPosition = 0;
	}

	preload() {

	}

	create() {
		console.log("Hello, title.");
		this.game.stage.backgroundColor = '#000000';
		this.texts.title = this.game.add.text(
			this.game.world.width / 2, this.game.world.height / 2,
			"Lunamalis",
			{
				font: "2em Arial",
				fill: "#eeeeee",
				align: "center"
			}
		);
		this.texts.title.anchor.setTo(0.5, 0.5);
		this.texts.title.addColor(TitleState.DefaultTitleColor, 0);

		this.texts.play = this.game.add.text(
			this.game.world.width / 2, (this.game.world.height / 2) + 50,
			"Play",
			{
				font: "1em Arial",
				fill: "#ffffff",
				align: "center"
			}
		);
		this.texts.play.inputEnabled = true;
		this.texts.play.events.onInputOver.add(this.play.bind(this));
	}

	update() {
		if (!this.titleTimeout) {
			for (let i = 0; i < TitleState.titleTextColors.length; i++) {
				const index = TitleState.wrapInt(i - this.titleTextColorPosition, TitleState.titleTextColors.length);
				const color = TitleState.titleTextColors[index];
				this.texts.title.addColor(color, i);
			}
			this.titleTextColorPosition += 1;
			this.titleTimeout = setTimeout(this._resetTimeout.bind(this), TitleState.titleSpeed);
		}
	}

	play(): void {
		this.game.state.start(StarmapState.NAME);
	}

	private _resetTimeout() {
		this.titleTimeout = null;
	}

	static wrapInt(value: number, maximum: number): number {
		if (value >= 0) {
			return value % maximum;
		}
		return ((value % maximum) + maximum) % maximum;
	}
}

interface ITitleTexts {
	title: Phaser.Text
	play: Phaser.Text
}
