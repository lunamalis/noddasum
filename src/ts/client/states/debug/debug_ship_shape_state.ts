import Phaser from 'phaser-ce'
import Client from '_/client/client_class'
import IPrShipType from '_/shared/interfaces/protocols/IPrShipType'
import deepFreeze from '_/shared/util/deepFreeze'

export = class DebugShipShapeState extends Phaser.State {
	static readonly NAME: string = "DebugShipShapeState"

	client: Client

	sprite: Phaser.Sprite

	DOMAIN: string
	NAME: string

	_debugKey: Phaser.Key

	_showDebug: boolean

	init() {
		this._showDebug = true;
		this.DOMAIN = this.client.QUERIES['debugShipDomain'];
		this.NAME = this.client.QUERIES['debugShipName'];

		this.client.socket.on("replyShipType", this._onReplyShipType.bind(this));
	}

	preload() {}

	create() {

		this._debugKey = this.game.input.keyboard.addKey(Phaser.Keyboard.UNDERSCORE)

		this.game.physics.startSystem(Phaser.Physics.P2JS);
		this.game.physics.p2.setImpactEvents(true);
		this.game.physics.p2.restitution = 0.8;

		this.game.physics.p2.gravity.y = 0;

		this.sprite = this.game.add.sprite(
			this.game.world.centerX,
			this.game.world.centerY,
			'abcd'
		);
		this.sprite.anchor.setTo(0.5, 0.5);
		this.game.physics.p2.enable(this.sprite);
		this.sprite.body.debug = true;
		this.game.debug.reset();
		// this.sprite.enableBody = true;
		// this.sprite.physicsBodyType = Phaser.Physics.P2JS;

		if (this.DOMAIN === undefined) {
			alert("Please specify a domain in the URL. Example: `&debugShipDomain=lunamalis`")
			throw new Error("Undefined ship domain.");
		}
		if (this.NAME === undefined) {
			alert("Please specify a name in the URL. Example: `&debugShipName=genericShip1`.")
			throw new Error("Undefined ship name.");
		}

		const sent = {
			domain: this.DOMAIN,
			name: this.NAME
		};
		console.log("Sending:", sent);
		this.client.socket.emit('understandShipType', sent);
	}

	update() {
		if (this._debugKey.justDown) {
			console.log(`Setting debug mode to ${!this._showDebug}`);
			this._showDebug = !this._showDebug;
			this.sprite.body.debug = this._showDebug;
			if (!this._showDebug) { this.game.debug.reset(); }
		}

		if (this.game.input.keyboard.isDown(Phaser.Keyboard.B)) {
			console.log(this.sprite.body);
		}

		if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
			this.sprite.body.x = this.game.world.centerX;
			this.sprite.body.y = this.game.world.centerY;
			this.sprite.body.rotation = 0;
		}

		if (this.game.input.keyboard.isDown(Phaser.Keyboard.SHIFT)) {
			this.sprite.body.setZeroVelocity();
			this.sprite.body.setZeroRotation();
		}

		if (this.game.input.keyboard.isDown(Phaser.Keyboard.W)) {
			this.sprite.body.thrust(100);
		}

		if (this.game.input.keyboard.isDown(Phaser.Keyboard.S)) {
			this.sprite.body.thrust(-100);
		}

		if (this.game.input.keyboard.isDown(Phaser.Keyboard.A)) {
			this.sprite.body.angularForce -= 5;
		}

		if (this.game.input.keyboard.isDown(Phaser.Keyboard.D)) {
			this.sprite.body.angularForce += 5;
		}
	}

	/**
	 * Called when the Server replies with a ship type.
	 */
	private _onReplyShipType(shipType: IPrShipType) {
		console.log(shipType, shipType.shape[0]);
		deepFreeze(shipType);
		const domain = shipType.domain;
		const name = shipType.name;
		const textureName: string = `ship--${domain}-${name}`;
		const texturePath: string = `resources/assets/${domain}/textures/${shipType.texture}.png`
		this.game.load.image(textureName, texturePath);
		this.game.load.start();

		this.game.load.onLoadComplete.add(() => {
			this.sprite.loadTexture(textureName);
			const b = <Phaser.Physics.P2.Body> this.sprite.body;
			b.clearShapes();
			b.addPolygon({}, shipType.shape.slice());
			b.x = this.game.world.centerX;
			b.y = this.game.world.centerY;
		});

	}
}
