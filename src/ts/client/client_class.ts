import CONFIG from '_co';
import Phaser from 'phaser-ce';
import TitleState from './states/title_state';
import StarmapState from './states/starmap_state';
import socketio from 'socket.io';
import BattleState from './states/battle_state';
import deepFreeze from '_/shared/util/deepFreeze';
import DebugShipShapeState from './states/debug/debug_ship_shape_state';


export default class Client {

	static exists: boolean = false

	game: Phaser.Game
	socket: socketio.Socket

	readonly QUERIES: Readonly<{[key: string]: string}>

	constructor() {
		if (Client.exists) {
			throw new Error("Client already exists! Only one Client may exist.");
		}
		Client.exists = true;

		this.QUERIES = deepFreeze(this._get_queries());
	}

	start() {
		this.game = new Phaser.Game(
			800,
			400,
			Phaser.AUTO,
			'mainCanvas'
		);

		// @ts-ignore
		this.socket = io();

		this.game.state.add(TitleState.NAME, new TitleState());
		this.game.state.add(StarmapState.NAME, new StarmapState());
		this.game.state.add(BattleState.NAME, new BattleState());
		if (this.QUERIES['debug'] !== undefined) {
			this.game.state.add(DebugShipShapeState.NAME, new DebugShipShapeState());
		}

		console.log("States number: ");
		// for (let i = 0; i < this.game.state.states.length; i++) {
		for (const [key, value] of Object.entries(this.game.state.states)) {
			// @ts-ignore
			value.client = this;
		}

		if (this.QUERIES['autostartState'] === undefined) {
			this.game.state.start(TitleState.NAME);
		}
		else {
			this.game.state.start(this.QUERIES['autostartState']);
		}
	}

	private _get_queries(): {[key: string]: string} {
		const queries: {[key: string]: string} = {};

		for (const query of window.location.search.substr(1).split('&')) {
			const [key, value] = query.split('=');
			if (value !== undefined) {
				// Consider using `isNaN` to auto cast into numbers.
				queries[key] = window.decodeURIComponent(value);
			}
			else {
				// TODO: not so good
				queries[key] = "true";
			}
		}

		return queries;
	}
}
