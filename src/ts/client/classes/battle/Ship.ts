import BattleState from "_/client/states/battle_state";
import Phaser from 'phaser-ce';
import IPrShipType from "_/shared/interfaces/protocols/IPrShipType";

export = class Ship {

	id: number
	battle: BattleState
	game: Phaser.Game
	sprite: Phaser.Sprite
	shipType: IPrShipType

	constructor(battle: BattleState, id: number, group: Phaser.Group, collision: Phaser.Physics.P2.CollisionGroup, shipType: Readonly<IPrShipType>) {
		this.battle = battle;
		this.id = id;
		this.game = battle.game;
		this.shipType = shipType;

		this.sprite = group.create(
			Math.floor(Math.random() * 50) + 50,
			50,
			`ship--${this.shipType.domain}-${this.shipType.name}`
		);
		const s = this.sprite;
		this.game.physics.enable(s, Phaser.Physics.P2JS);
		const b = <Phaser.Physics.P2.Body> s.body;
		console.log(this.shipType);
		b.addPolygon({}, this._copy_shape());
		b.setCollisionGroup(collision);
		b.collides([collision]);
		b.angularDamping = 0.1;
	}

	private _copy_shape(): Array<[number, number]> {
		const returner = [];
		for (const i of this.shipType.shape) {
			returner.push(i.slice());
		}
		return returner;
	}
}
