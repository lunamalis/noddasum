import Phaser from 'phaser-ce';
import StarmapState from '_c/states/starmap_state';
export = class Star {
	state: StarmapState
	graphics: Phaser.Graphics

	sprite: Phaser.Sprite
	position: Phaser.Point
	id: number
	hyperlanes: Array<number>
	active: boolean

	constructor(state: StarmapState, id: number, point: Phaser.Point) {
		this.hyperlanes = [];
		this.state = state;
		this.position = point;
		this.id = id;

		// this.id = this.state.newStarId();

		const sprite = this.state.game.add.sprite(point.x, point.y, this.generateTexture());
		sprite.data.star = this;
		sprite.anchor.setTo(0.5, 0.5);
		sprite.inputEnabled = true;
		sprite.events.onInputDown.add(this.onClick.bind(this));
		this.sprite = sprite;
	}

	generateTexture(color: number = 0xffffff, diameter: number = 10): Phaser.RenderTexture {
		const graphics = new Phaser.Graphics(this.state.game, 0, 0);
		graphics.beginFill(color)
			.drawCircle(0, 0, diameter)
			.endFill();

		const returner: Phaser.RenderTexture = graphics.generateTexture();
		graphics.destroy();
		return returner;
	}

	/**
	 * "Activate" this star, making it active.
	 */
	activate() {
		this.sprite.loadTexture(this.generateTexture(0x0000ff));
	}

	deactivate() {
		this.sprite.loadTexture(this.generateTexture());
	}

	onClick() {
		this.activate();
		this.state.starsById[this.state.activeStar].deactivate();
		this.state.activeStar = this.id;
	}
}
