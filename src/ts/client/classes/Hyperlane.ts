import Phaser from 'phaser-ce';
import Star from './Star';
export = class Hyperlane {

	stars: [Star, Star]
	line: Phaser.Line

	constructor(stars: [Star, Star]) {
		this.stars = stars;
		this.line = new Phaser.Line();
	}
}
