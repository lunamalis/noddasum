import BaseManager from '_/server/base_manager';
import Server from '_/server/Server';
import BasePage from '_/server/base_page';
export default class PageManager extends BaseManager {
	routes: Record<string, BasePage>
	constructor(server: Server) {
		super(server);
		this.routes = {};
	}

	/**
	 * Add a new page to the manager.
	 * @param page The Page to add.
	 */
	add_page(page: BasePage) {
		this.log.info("Adding new page at " + page.route + ".");
		this.routes[page.route] = page;
	}

	finish() {
		for(const [route, page] of Object.entries(this.routes)) {
			this.log.info("Setting up " + route + ".");
			page.setup(this.server.app);
		}
	}
}
