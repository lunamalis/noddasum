import Server from "../Server";
import Mod from '_/shared/interfaces/Mod';
import RegistryManager from "./registry_manager";
import IShipType from "../interfaces/IShipType";
import log4js from "log4js";
import BaseManager from "../base_manager";


export default class ModManager extends BaseManager {
	server: Server
	readonly MOD_DIR: string
	mods: { [key: string]: Mod }
	registry: RegistryManager
	log: log4js.Logger

	constructor(server: Server) {
		super(server);
		this.MOD_DIR = this.server.path.MOD_DIR;
		this.registry = this.server.registry_manager;
		// this.mods = mod_list;
		this.mods = {}
	}

	get_mods(): void {
		// This is necessary here because the compiler forgets to add it itself.
		// For some reason.
		const mods_list: {[key: string]: Mod} = require('_sh/mods/mod_list').default;
		for (const [mod_key, mod_value] of Object.entries(mods_list)) {
			// @ts-ignore
			this.mods[mod_value.NAME] = new mod_value(this);
		}
		this.log.info(`Loaded ${this.length} mods.`);
	}

	load_all(): void {
		for (const mod of Object.values(this.mods)) {
			mod.build();
		}
		for (const mod of Object.values(this.mods)) {
			mod.preInit();
		}
		for (const mod of Object.values(this.mods)) {
			mod.init();
		}
		for (const mod of Object.values(this.mods)) {
			mod.postInit();
		}
	}

	get length(): number {
		return Object.keys(this.mods).length - 1;
	}

	registerShip(obj: IShipType) {
		this.registry.addShip(obj);
	}
}
