import path from "path"
import BaseManager from "../base_manager"
import Server from "../Server"

class PathManager extends BaseManager {

	readonly ABSOLUTE_DIR = __dirname
	readonly ROOT_DIR = path.dirname(this.ABSOLUTE_DIR)
	readonly REPO_DIR = path.dirname(this.ROOT_DIR)
	readonly PUBLIC_DIR = path.join(this.ROOT_DIR, "public")
	readonly NODE_MODULES_DIR = path.join(this.REPO_DIR, "node_modules")
	readonly RESOURCES_DIR = path.join(this.REPO_DIR, "resources")
	readonly MOD_DIR = path.join(this.ROOT_DIR, "shared", "mods")
	readonly LOG_DIR = path.join(this.REPO_DIR, "log")

	constructor(server: Server) {
		super(server);
	}
}
export default PathManager
