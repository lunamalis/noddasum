import Server from "_s/Server";
import IShipType from "../interfaces/IShipType";

export default class RegistryManager {

	server: Server

	readonly data: {
		ship: {
			[domain: string]: {
				[name: string]: {
					domain: string
					name: string
					shape: Array<[number, number]>
					texture: string
				}
			}
		}
	}

	constructor(server: Server) {
		this.server = server;
		this.data = {
			ship: {}
		};
	}

	freeze(): void {
		Object.freeze(this.data);
	}

	addShip(obj: IShipType): void {
		if (this.data.ship[obj.domain] === undefined) {
			this.data.ship[obj.domain] = {};
		}
		this.data.ship[obj.domain][obj.name] = obj;
	}
}
