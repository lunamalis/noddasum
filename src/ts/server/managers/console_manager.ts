import repl from 'repl';
import Server from "../Server";

export default class ConsoleManager {
	readonly server: Server


	constructor(server: Server) {
		this.server = server;
	}

	get_commands(context: import("vm").Context) {
		const c = context;
		for (const func of [
			// eslint-disable-next-line @typescript-eslint/unbound-method
			this._stop
		]) {
			// Ignore the preceding `_`.
			c[func.name.substring(1)] = func.bind(this)
		}
	}

	private _stop(): void {
		console.log("Stopping.");
		this.server.stop();
	}
}
