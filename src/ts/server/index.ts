// Entry file.
import Server from '_/server/Server';

/**
 * This is where everything happens.
 *
 * This is the entry point to the program.
 */
function main() {
	const s = new Server();
	s.start();
}

// This is the real entry point.
// ...
// But does it matter?
// Code never lies, comments do.
if (require.main === module) {
	main();
}
