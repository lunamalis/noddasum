import BasePage from '_/server/base_page';

export default class APIPage extends BasePage {
	route: string = '/api'
	setup(app) {
		app.get(this.route, this.get.bind(this));
	}

	get(req, res) {
		res.send("Greetings.")
	}

}
