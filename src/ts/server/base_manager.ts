import Server from '_/server/Server'
import log4js from 'log4js';

export default class BaseManager {
	server: Server

	log: log4js.Logger

	constructor(server: Server) {
		this.server = server;
		this.log = this.server.log;
	}
}
