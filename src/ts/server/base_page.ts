abstract class BasePage {
	abstract route: string

	abstract setup(app: import('express').Express): void
}

export default BasePage;
