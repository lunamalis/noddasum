import IPlanet from "./IPlanet";
// Recommend looking at:
// https://www.astronomynotes.com/solarsys/plantbla.htm
interface IOrbit {
	/**
	 * The time it takes to revolve once around its center.
	 */
	revolution: number

	/**
	 * The distance from its center.
	 */
	distance: number

	/**
	 * The eccentricity of its orbit.
	 */
	eccentricity: number

	/**
	 * The inclination of the orbit, in degrees.
	 */
	inclination: number
	child: IPlanet
}
export default IOrbit;
