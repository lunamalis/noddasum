// Interface for a Planet stored in JSON
interface IPlanet {
	/**
	 * How much the planet weighs in Earths.
	 * Earth would be equal to 1.0.
	 */
	mass: number

	/**
	 * The radius of the planet is kilometers.
	 */
	radius: number

	/**
	 * The density of the planet in grams per cubic centimeter.
	 */
	density: number

	/**
	 * The "roundness" of a planet. How much it bulges at the equator.
	 */
	oblateness: number

	/**
	 * How long it takes for this planet to rotate once, in Earth days.
	 */
	rotation: number

	/**
	 * The tilt of the planet, in degrees.
	 */
	axis_tilt: number
}
export default IPlanet
