import StarType from '_s/enums/StarType';
import StarClass from '../enums/StarClass';
// Interface for a Star stored in JSON
interface IStar {
	/**
	 * The identification number.
	 * Completely unique.
	 */
	id: number

	/**
	 * The age of the star, measured in millions of years.
	 */
	age: number

	/**
	 * One of the most important properties of a star.
	 */
	mass: number

	/**
	 * Generated.
	 * Uses formula:
	 * `( Min + ((A/S) * (Max - Min)) ) + (random 10% in either direction)`
	 */
	temperature: number

	luminosity: number

	radius: number

	type: StarType

	class: StarClass
}
export default IStar
