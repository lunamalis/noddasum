import IStar from "./IStar";

interface IGalaxy {
	stars: Array<IStar>
}

export default IGalaxy;
