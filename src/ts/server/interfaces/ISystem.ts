import IStar from "./IStar"
import IOrbit from "./IOrbit"

// A System is a collection of items close to each other.
interface ISystem {
	center: IStar
	children: Array<IOrbit>
}
export default ISystem;
