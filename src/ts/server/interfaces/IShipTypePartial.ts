interface IShipTypePartial {
	name: string
	shape: Array<Array<number>>
	texture: string
}


export default IShipTypePartial
