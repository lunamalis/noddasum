import IGalaxy from "../interfaces/IGalaxy";


/**
 * This class generates `Universe`s.
 */
export default class UniverseGenerator {

	width: number
	height: number
	galaxy_number: number
	galaxies: Array<IGalaxy>

	constructor(width: number = 200, height: number = 200, galaxies: number = 1) {
		this.width = width;
		this.height = height;
		this.galaxy_number = galaxies;
		this.galaxies = [];
	}
}
