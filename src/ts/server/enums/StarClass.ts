enum StarClass {
	WHITE_DWARF,
	SUBDWARF,
	MAIN_SEQUENCE,
	GIANT_1A,
	GIANT_1B,
	GIANT_2,
	GIANT_3,
	SUBGIANT
}
export default StarClass;
