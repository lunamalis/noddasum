enum StarType {
	O = "BLUE",
	B = "BLUE_WHITE",
	A = "WHITE",
	F = "YELLOW_WHITE",
	G = "YELLOW",
	K = "ORANGE",
	M = "RED"
}
export default StarType;
