/**
 * This is the Server class file.
 * @packageDocumentation
 */
import path from 'path';
import express from 'express';
import socketio from 'socket.io';
import log4js from 'log4js';
import CONFIG from '_co';

import PageManager from '_/server/managers/page_manager';
import APIPage from '_/server/pages/api';
import http from 'http';
import MapData from '_/shared/interfaces/protocols/IMapData';
import ModManager from './managers/mod_manager';
import IPrBattleInit from '_/shared/interfaces/protocols/IPrBattleInit';
import IPrShipType from '_/shared/interfaces/protocols/IPrShipType';
import RegistryManager from './managers/registry_manager';
import IShipType from './interfaces/IShipType';
import repl from 'repl';
import ConsoleManager from './managers/console_manager';
import fs from 'fs';
import PathManager from './managers/path_manager';

/**
 * This is the main class for the server. Everything occurs here.
 */
export default class Server {
	/**
	 * The HTTP server.
	 */
	readonly http: http.Server

	/**
	 * The Socket.IO server.
	 */
	readonly io: socketio.Server

	/**
	 * The Express application server.
	 */
	readonly app: express.Express

	/**
	 * The Mod manager.
	 */
	readonly mod_manager: ModManager

	/**
	 * The Page manager.
	 */
	readonly page_manager: PageManager

	/**
	 * The Registry manager (holds ships, etc).
	 */
	readonly registry_manager: RegistryManager

	/**
	 * The Console manager (handles the REPL).
	 *
	 * Initialized, but not started if the script is run like:
	 * `./scripts/run.bash --no-repl`.
	 */
	readonly console_manager: ConsoleManager

	/**
	 * The socket clients from socket.io.
	 */
	readonly clients: {[key: string]: socketio.Socket}

	/**
	 * This is the primary logger used.
	 */
	readonly log: log4js.Logger

	/**
	 * This contains paths commonly used.
	 */
	readonly path: PathManager

	/**
	 * Heartbeats. INDEV
	 */
	readonly heartBeats: {[socket_id: string]: Date}

	/**
	 * A `Date` object created when the Server starts.
	 */
	readonly timeStarted: Date

	/**
	 * The Express part of the server.
	 */
	private _expressListenServer: http.Server


	/**
	 * The constructor.
	 */
	constructor() {

		this.timeStarted = new Date();
		console.log(this.timeStarted);

		this.app = express();
		this.clients = {};

		this.path = new PathManager(this);

		this.log = this._get_logger(this.timeStarted);

		if (!fs.existsSync(this.path.LOG_DIR)) {
			fs.mkdirSync(this.path.LOG_DIR);
		}

		this.http = http.createServer(this.app);
		this.io = socketio(this.http);

		this.registry_manager = new RegistryManager(this);
		this.mod_manager = new ModManager(this);
		this.mod_manager.get_mods();
		this.mod_manager.load_all();

		this.registry_manager.freeze();

		this.page_manager = new PageManager(this);
		this.page_manager.add_page(new APIPage());
		this.page_manager.finish();
		this.log.info("Public path is at:", this.path.PUBLIC_DIR);

		this.console_manager = new ConsoleManager(this);

		this.app.use(express.static(this.path.PUBLIC_DIR));
		this.app.use('/resources/', express.static(this.path.RESOURCES_DIR));
		this.app.use('/lib/phaser.js', express.static(path.join(
			this.path.NODE_MODULES_DIR,
			'phaser-ce',
			'build',
			'phaser.js'
		)));
		this.app.use('/client.js', express.static(path.join(
			this.path.ROOT_DIR,
			'client',
			'main.js'
		)));

		this.io.on('connection', this.onConnection.bind(this));
		this.io.on('disconnect', this.onDisconnect.bind(this));

		if (!process.argv.includes("--no-repl")) {
			console.log("Type `.stop` to stop.");
			console.log("Or, press ^C twice to immediately terminate.");
			const replServer: repl.REPLServer = repl.start({
				prompt: "noddasum> ",
				ignoreUndefined: false
			});

			replServer.defineCommand("stop", () => {
				console.log("Cya.");
				this.stop();
			})

			replServer.on("exit", () => {
				console.log("Cya.");
				this.stop();
			});

			replServer.context.server = this;
			replServer.context.cmd = this.console_manager;
			this.console_manager.get_commands(replServer.context);
		}
	}

	/**
	 * The entry point for the Server class.
	*/
	start() {
		this._expressListenServer = this.http.listen(CONFIG.PORT);
	}

	/**
	 * Called when the Server stops.
	 */
	stop() {
		this._expressListenServer.close();
		this.log.info("Server closed. Exiting process.");
		this.log.info("If the REPL does not close on its own, press ^C twice or type `.exit`.");
		process.exit(0);
	}

	/**
	 * Called when the Server makes a new socket connection.
	 *
	 * @param socket The socket.
	 */
	onConnection(socket: socketio.Socket) {
		this.log.info("Connected", socket.id, ".");
		this.clients[socket.id] = socket;
		socket.on('message', function(message){
			this.log.info('message: ', message)
		});

		socket.on('requestMap', this._onRequestMap.bind(this, socket));
		socket.on('battleInit', this._onBattleInit.bind(this, socket));
		socket.on('understandShipTypes', this._onUnderstandShipTypes.bind(this, socket));
		socket.on('battleReady', this._onBattleReady.bind(this, socket));
		socket.on('understandShipType', this._onUnderstandShipType.bind(this, socket));
	}

	/**
	 * Called when the Server disconnects a socket.
	 * @param socket The socket.
	 */
	onDisconnect(socket: socketio.Socket) {
		this.log.info("Goodbye socket", socket.id, ".");
		delete this.clients[socket.id];
	}

	/**
	 * Called when a client requests a Socket.
	 * @param socket The socket provided.
	 */
	private _onRequestMap(socket: socketio.Socket) {
		this.log.info("Requested map.");
		const data: MapData = {
			stars: {
				0: {
					id: 0,
					x: 100,
					y: 100
				}
			},
			position: 0
		}
		socket.emit("sendMap", data);
	}

	/**
	 * The "Battle init" message is used to create the battle.
	 * Ship data is sent.
	 * @param socket The socket provided.
	 */
	private _onBattleInit(socket: socketio.Socket) {
		this.log.info("Requested battle initialization.");
		socket.emit("replyBattleInit", <IPrBattleInit> {
			lunamalis: [
				"genericShip1",
				"genericShip2"
			]
		});
	}

	/**
	 * Called automatically when the client requests ship data.
	 * @param socket The provided socket.
	 * @param needed The requested ship ids.
	 */
	private _onUnderstandShipTypes(socket: socketio.Socket, needed: IPrBattleInit) {
		const sender: {[domain: string]: {[id: string]: IPrShipType}} = {};
		for (const [domain, ids] of Object.entries(needed)) {
			// sender.push(this._shipTypeToClient(this.registry_manager.data.ship[shipType]));
			sender[domain] = {};
			for (const id of ids) {
				sender[domain][id] = this._shipTypeToClient(this.registry_manager.data.ship[domain][id]);
				this.log.info("Doing id, got", sender[domain][id]);
			}
		}
		socket.emit("replyShipTypes", sender);
	}

	/**
	 * Called automatically when a client requests a single ship type.
	 * @param socket The provided socket.
	 * @param needed The requested ship id.
	 */
	private _onUnderstandShipType(socket: socketio.Socket, needed) {
		this.log.info("Requested to understand ship type:", needed);
		const sender: IPrShipType = this._shipTypeToClient(
			this.registry_manager.data.ship[needed.domain][needed.name]
		);
		socket.emit("replyShipType", sender);
	}

	/**
	 * Returns ship data, just for clients.
	 * @param shipType Ship data.
	 */
	private _shipTypeToClient(shipType: IShipType): IPrShipType {
		return {
			domain: shipType.domain,
			name: shipType.name,
			shape: shipType.shape,
			texture: shipType.texture
		};
	}

	/**
	 * Called automatically when a client says that they are ready to launch the battle.
	 * @param socket The provided socket.
	 */
	private _onBattleReady(socket: socketio.Socket) {
		// Give them a bunch of ships.
		socket.emit('battleCreate', [
			{
				id: 0,
				domain: "lunamalis",
				name: "genericShip1",
				x: 10,
				y: 10
			},
			{
				id: 1,
				domain: "lunamalis",
				name: "genericShip1",
				x: 20,
				y: 50
			},
			{
				id: 2,
				domain: "lunamalis",
				name: "genericShip2",
				x: 30,
				y: 100
			}
		]);
	}

	private _get_logger(timeStarted: Date): log4js.Logger {

		log4js.configure({
			appenders: {
				filelog: {
					type: "file",
					filename: path.join(this.path.LOG_DIR, "log.log"),
					layout: Server.getLoggerLayout(timeStarted, false)
				},
				console: {
					type: "console",
					layout: Server.getLoggerLayout(timeStarted, true)
				}
			},
			categories: {
				"lunamalis/noddasum": {
					level: "error",
					appenders: ["filelog", "console"]
				},
				"lunamalis/noddasum/mod": {
					level: "error",
					appenders: ["filelog", "console"]
				},
				default: {
					level: "error",
					appenders: ["console"]
				}
			}
		});

		const log = log4js.getLogger("lunamalis/noddasum");

		// TODO: for now
		log.level = 'debug';

		log.debug("Logger set up, returning.");

		// const testlog = log4js.getLogger("lunamalis/noddasum/mod");
		// testlog.error("?");

		return log;
	}

	/**
	 * This is an evil hack, and I'm not sure I want this to be used.
	 *
	 * Essentially, it makes the logger global.
	 * It isn't used anywhere, but it could in the future.
	 */
	private _globalize_logger(log_: log4js.Logger) {
		// @ts-ignore
		global.log = log_;
	}

	static getLoggerLayout(timeStarted: Date, with_color: boolean = false) {
		let pattern: string;
		if (with_color) {
			pattern = "%x{time} %[[%p]%] %c> %m";
		}
		else {
			pattern = "%x{time} [%p] %c> %m";
		}
		return {
			type: "pattern",
			pattern: pattern,
			tokens: {
				time: function(logEvent) {
					return Server.padLogTime((new Date().getTime() - timeStarted.getTime()), 5)
				}
			}
		};
	}

	static padLogTime(num: number, len: number): string {
		// TODO: bad

		let str = Math.floor(num / 1000).toString();
		while (str.length < len) {
			str = "0" + str;
		}
		let extra = (num / 1000).toString().split('.')[1];
		while (extra.length < 3) {
			extra = extra + "0";
		}
		return str + "." + extra;
	}

}
