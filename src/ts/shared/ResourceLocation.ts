import path from "path"
import ResourceEnum from "./enums/ResourceEnum";

export default class ResourceLocation {

	private readonly _location: string

	constructor(resourcePath: string, domain: string, resourceType: ResourceEnum, location: string) {
		this._location = path.join(resourcePath, 'assets', domain, resourceType, location)
	}

	get location(): string {
		return this._location
	}

	toString() {
		return this._location
	}
}
