export default function deepFreeze(obj) {
	Object.freeze(obj);
	for (const property of Object.getOwnPropertyNames(obj)) {

		const t = typeof obj[property];

		if (t === "object" || t === "function") {
			deepFreeze(obj[property]);
		}
	}

	return obj;
}
