import Mod from "_/shared/interfaces/Mod";

export default class Lunamalis extends Mod {
	static readonly NAME: Readonly<string> = "lunamalis"
	static readonly VERSION: Readonly<string> = "0.0.1"
	static readonly VERSION_NUMBER: Readonly<number> = 0
	static readonly FRIENDLY_NAME: Readonly<string> = "Lunamalis"
	static readonly DESCRIPTION: Readonly<string> = "The base game for Noddasum."
	static readonly NODDASUM_VERSION: ReadonlyArray<string> = ["*"]
	static readonly AUTHOR: ReadonlyArray<string> = ["Clothlen"]
	static readonly CREDITS: Readonly<string> = "You-know-who."

	preInit() {}
	init() {


		this.registerShip({
			name: "genericShip1",
			shape: [
				[0, 0],
				[200, 0],
				[200, 200],
				[0, 200]
			],
			texture: "F5S1"
		});
		this.registerShip({
			name: "genericShip2",
			shape: [
				[0, 0],
				[50, 10],
				[100, 0],
				[100, 100],
				[0, 100]
			],
			texture: "F5S2"
		});
		this.registerShip({
			name: "faction5ship1",
			// shape: [
			// 	[0, -30], // entry
			// 	[20, -30],
			// 	[20, 10],
			// 	[20, 20],
			// 	[40, 20],
			// 	[40, 0],
			// 	[50, 0],
			// 	[50, 70],
			// 	[0, 140],  // center
			// 	[-50, 70],
			// 	[-50, 0],
			// 	[-40, 0],
			// 	[-40, 20],
			// 	[-20, 20],
			// 	[-20, 10],
			// 	[-20, -30]
			// ],
			// shape: this._symmetrical([
			// 	// [0, -30], // entry
			// 	// [20, -30],
			// 	// [20, 10],
			// 	// [20, 20],
			// 	// [40, 20],
			// 	// [40, 0],
			// 	// [50, 0],
			// 	// [50, 70]
			// 	// entry
			// 	[0, 0],
			// 	[5, 5]
			// ]),
			// shape: this._symmetrical([0, 0], [  // cool design but not for this one
			// 	[2.8, 3],
			// 	[2, 4],
			// 	[2, 3],
			// 	[0, 10, 0]
			// ]),
			shape: this._symmetrical([
				[0, 0, 0],
				[2.5, 3],
				[2.5, 5.5],
				[2, 5.5],
				[2, 4.5],
				[0, 10, 0]
			]),
			texture: "F5S1"
		});

		this.registerShip({
			name: "faction5ship2",
			shape: this._symmetrical([
				[0, -3.2, 0],
				[1.8, -2.3],
				[2.5, 2],
				[2.84, -3.4],  // the center bottom protrusion
				[3.2, -2.2],  // the center bottom outward protrusion
				[2.5, 4.4],
				[1.8, 5.5],
				[1.9, 1], // the inward protrusion in the ship
				[0.5, 6.3]  // the flat nose
			]),
			texture: "F5S2"
		});
	}
	postInit() {}

	private _symmetrical(shape: Array<[number, number, number?]>): Array<[number, number]> {
		const newshape = shape.slice();

		const mirrored = shape.slice().reverse();
		for (const val of mirrored) {
			if (val.length === 2) {
				newshape.push([-val[0], val[1]]);
			}
		}

		newshape.concat(mirrored);

		return <Array<[number, number]>> newshape;
	}
}
