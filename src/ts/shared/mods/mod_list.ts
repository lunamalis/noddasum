import Mod from "../interfaces/Mod";

// Add your mods here.
const mods: Array<string> = ["lunamalis"];

// Don't need to touch anything below here.
const mods_out: {[key: string]: Mod} = {};
for (const mod of mods) {
	// This `.default` is necessary for some reason.
	// Normally, the compilation adds it by itself, normally.
	// But it forgets to do so here for some reason.
	const mod_required = <Mod> (require(`./${mod}/__init__`).default);
	// Now let's check that mod.
	const m = mod_required;
	mods_out[mod] = m;
	for (const item of [
		"NAME",
		"VERSION",
		"VERSION_NUMBER",
		"FRIENDLY_NAME",
		"DESCRIPTION",
		"NODDASUM_VERSION",
		"AUTHOR",
		"CREDITS"
	]) {
		if (m[item] === undefined) {
			throw new Error(`Invalid Mod class. Item ${item} was not found on class ${m}. Mod id: ${mod}`);
		}
	}
}


export default mods_out;
