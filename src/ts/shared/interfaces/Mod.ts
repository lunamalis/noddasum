import ModManager from "_/server/managers/mod_manager";
import IShipType from "_/server/interfaces/IShipType";
import IShipTypePartial from "_/server/interfaces/IShipTypePartial";

abstract class Mod {
	/**
	 * The internal name of your mod.
	 * Use all lowercase. ALphanumeric.
	 * Do not start it with a number.
	 */
	static readonly NAME: Readonly<string>

	/**
	 * The version of your mod.
	 */
	static readonly VERSION: Readonly<string>

	/**
	 * The version of your mod, represented as an integer.
	 */
	static readonly VERSION_NUMBER: Readonly<number>

	/**
	 * The "friendly" version of your mod.
	 * Can be anything you'd like.
	 */
	static readonly FRIENDLY_NAME: Readonly<string>

	/**
	 * The description of your mod.
	 */
	static readonly DESCRIPTION: Readonly<string>

	/**
	 * The versions of Noddasum that your mod is compatible with.
	 * Not really used.
	 */
	static readonly NODDASUM_VERSION: ReadonlyArray<string>

	/**
	 * A list of authors of your mod.
	 */
	static readonly AUTHOR: ReadonlyArray<string>

	/**
	 * Any extra things you'd like to say.
	 */
	static readonly CREDITS: Readonly<string>

	readonly mana: ModManager

	constructor(mana: ModManager) {
		this.mana = mana;
	}

	/**
	 * Here, set up extra stuff, before mod loading.
	 * Do *not* do things like read configurations here.
	 * Do that later.
	 */
	build(): void {}

	registerShip(obj: IShipTypePartial) {
		this.mana.registry.addShip(<IShipType> {
			domain: (<typeof Mod>this.constructor).NAME,
			name: obj.name,
			shape: obj.shape,
			texture: obj.texture
		});
	}

	/**
	 * Here, read your configs and whatever.
	 */
	abstract preInit(): void

	/**
	 * Register ships and whatnot.
	 */
	abstract init(): void

	/**
	 * Here, do anything that requires functionality based off of other mods.
	 */
	abstract postInit(): void

}

export default Mod;
