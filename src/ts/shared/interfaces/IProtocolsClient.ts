import IProtocolsCommon from './IProtocolsCommon';

// Use `keyof IProtocolsClient`?
/**
 * These are messages that the client sends to the server.
 */
interface IProtocolsClient extends IProtocolsCommon {
	"battle.init": () => void
	"battle.ready": () => void
}

export default IProtocolsClient
