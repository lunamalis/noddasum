// interface IProtocolInit {
// 	name: string
// 	receivers: {
// 		[key: string]: Function
// 	}
// 	senders: {
// 		[key: string]: Function
// 	}
// }
interface IProtocolInit<A, B> {
	name: string
	receivers: A
	senders: B
}

export default IProtocolInit
