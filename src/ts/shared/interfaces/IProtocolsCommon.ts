/**
 * These are message types that can be sent to and from the server and client.
 */
interface IProtocolsCommon {
	/**
	 * Heartbeat.
	 *
	 * Called occasionally so that the client and server still know each other
	 * are there.
	 */
	"heartbeat": () => void

	"map.request": () => void
	"understand.shipTypes": () => void
	"understand.shipType": () => void

}

export default IProtocolsCommon
