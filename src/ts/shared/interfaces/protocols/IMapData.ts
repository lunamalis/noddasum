interface IMapData {
	stars: {
		[id: number]: {
			id: number
			x: number
			y: number
		}
	}
	position: number
}
export default IMapData
