interface IPrShipType {
	domain: string
	name: string
	shape: Array<[number, number]>
	texture: string
}

export default IPrShipType
