import IBattleShip from "../IBattleShip";

// interface IPrBattleInit extends Array<{
// 	domain: string,
// 	id: string
// }> {}
interface IPrBattleInit {
	[domain: string]: Array<string>
}

export default IPrBattleInit
