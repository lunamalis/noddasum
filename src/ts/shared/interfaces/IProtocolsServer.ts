import IProtocolsCommon from './IProtocolsCommon';


/**
 * These are messages that the server sends to the client.
 */
interface IProtocolsServer extends IProtocolsCommon {
	"connect": () => void
	"disconnect": () => void
}

export default IProtocolsServer
