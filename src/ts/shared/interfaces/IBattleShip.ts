// A Ship seen in battles.
interface IBattleShip {
	/**
	 * The ship identifier.
	 */
	readonly id: number

	/**
	 * The domain of the ship.
	 * (Mod name.)
	 */
	readonly shipDomain: string

	/**
	 * The identifier for the ship's type.
	 */
	readonly shipId: string

	/**
	 * The inital position, x.
	 */
	readonly x: number

	/**
	 * The inital position, y.
	 */
	readonly y: number

	readonly overrides?: {}
}
export default IBattleShip
