const data = {
	// Make sure that this is equivalent to `version` in `package.json`.
	VERSION: "0.0.1",
	PORT: 8090
};

export default Object.freeze(data);
