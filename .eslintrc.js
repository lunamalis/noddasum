// Also note:
// https://github.com/AtomLinter/linter-eslint/issues/200
module.exports = {
	"env": {
		"browser": true,
		"node": true
	},
	"parser": "@typescript-eslint/parser",
	"parserOptions": {
		"project": "tsconfig.json",
		"sourceType": "module"
	},
	"plugins": [
		"@typescript-eslint",
		"@typescript-eslint/tslint"
	],
	"rules": {
		"@typescript-eslint/array-type": [
			"warn",
			{
				default: "generic"
			}
		],
		"@typescript-eslint/class-name-casing": "warn",
		"@typescript-eslint/indent": [
			"warn",
			"tab"
		],
		"@typescript-eslint/interface-name-prefix": [
			"warn",
			{ "prefixWithI": "always" }
		],
		"@typescript-eslint/member-ordering": "warn",
		"@typescript-eslint/no-empty-interface": "warn",
		"@typescript-eslint/no-namespace": "error",
		"@typescript-eslint/prefer-for-of": "warn",
		"@typescript-eslint/unbound-method": [
			"warn",
			{
				"ignoreStatic": true
			}
		],
		"@typescript-eslint/adjacent-overload-signatures": "warn",
		// produces a warning for `call(<number>3)` for example
		// "@typescript-eslint/consistent-type-assertions": [
		// 	"warn",
		// 	{
		// 		"assertionStyle": "angle-bracket",
		// 		"objectLiteralTypeAssertions": "never"
		// 	}
		// ],
		"@typescript-eslint/consistent-type-definitions": ["error", "interface"],
		"@typescript-eslint/explicit-member-accessibility": [
			"warn",
			{
				"accessibility": "no-public"
			}
		],
		"@typescript-eslint/member-delimiter-style": [
			"warn",
			{
				"multiline": {
					"delimiter": "none"
				},
				"singleline": {
					"delimiter": "semi"
				}
			}
		],
		"@typescript-eslint/member-ordering": [
			"warn",
			{
				"default": [
					"public-static-field",
					"protected-static-field",
					"private-static-field",

					"public-abstract-field",
					"protected-abstract-field",
					"private-abstract-field",

					"public-instance-field",
					"protected-instance-field",
					"private-instance-field",

					"public-constructor",
					"protected-constructor",
					"private-constructor",

					"public-instance-method",
					"protected-instance-method",
					"private-instance-method",

					"public-abstract-method",
					"protected-abstract-method",
					"private-abstract-method",

					"public-static-method",
					"protected-static-method",
					"private-static-method",

					"signature"

				]
			}
		],

		// Does not seem to work sometimes, such as:
		// _symmetrical
		// "@typescript-eslint/naming-convention": [
		// 	"warn",
		// 	{
		// 		"selector": "default",
		// 		"format": ["snake_case"],
		// 		"leadingUnderscore": "allow",
		// 		"trailingUnderscore": "allow"
		// 	},
		// 	{
		// 		"selector": "variable",
		// 		"format": ["snake_case"]
		// 	},
		// 	{
		// 		"selector": "class",
		// 		"format": ["PascalCase"]
		// 	},
		// 	{
		// 		"selector": "function",
		// 		"format": ["camelCase"]
		// 	},
		// 	{
		// 		"selector": "property",
		// 		"format": ["camelCase", "UPPER_CASE"]
		// 	},
		// 	{
		// 		"selector": "parameter",
		// 		"format": ["snake_case"]
		// 	},
		// 	{
		// 		"selector": "parameterProperty",
		// 		"format": ["snake_case"]
		// 	},
		// 	{
		// 		"selector": "method",
		// 		"format": ["camelCase"]
		// 	},
		// 	{
		// 		"selector": "enum",
		// 		"format": ["PascalCase"]
		// 	},
		// 	{
		// 		"selector": "enumMember",
		// 		"format": ["UPPER_CASE"]
		// 	},
		// 	{
		// 		"selector": "typeAlias",
		// 		"format": ["PascalCase"],
		// 		"prefix": ["T"]
		// 	},
		// 	{
		// 		"selector": "typeParameter",
		// 		"format": ["camelCase"]
		// 	},
		// 	{
		// 		"selector": "memberLike",
		// 		"modifiers": ["private"],
		// 		"format": ["camelCase"],
		// 		"leadingUnderscore": "require"
		// 	},
		// 	{
		// 		"selector": "method",
		// 		"modifiers": ["private"],
		// 		"format": ["camelCase"],
		// 		"leadingUnderscore": "require"
		// 	}
		// ],
		"@typescript-eslint/member-naming": [
			"warn",
			{
				"private": "^_"
			}
		],
		"@typescript-eslint/no-extra-non-null-assertion": ["error"],
		"@typescript-eslint/no-floating-promises": ["error"],
		// Could potentially be useful? Just use `for i in range`ish.
		"@typescript-eslint/no-for-in-array": ["warn"],
		// sometimes catches on `this.func.bind(this)`
		// "@typescript-eslint/no-implied-eval": ["error"],
		"@typescript-eslint/no-misused-new": ["error"],
		"@typescript-eslint/no-misused-promises": ["error"],
		"@typescript-eslint/no-throw-literal": ["error"],
		"@typescript-eslint/no-unnecessary-condition": ["warn"],
		"@typescript-eslint/no-unnecessary-type-arguments": ["warn"],
		"@typescript-eslint/no-unnecessary-type-assertion": ["warn"],
		"@typescript-eslint/prefer-function-type": ["warn"],
		"@typescript-eslint/prefer-includes": ["warn"],
		"@typescript-eslint/prefer-nullish-coalescing": "warn",
		"@typescript-eslint/prefer-optional-chain": "warn",
		"@typescript-eslint/prefer-readonly": "warn",
		"@typescript-eslint/prefer-regexp-exec": "warn",
		"@typescript-eslint/prefer-string-starts-ends-with": "warn",
		"@typescript-eslint/require-array-sort-compare": "warn",
		// Things like `window.thing = window.thing || {}` are good.
		// "@typescript-eslint/strict-boolean-expressions": "warn",
		"@typescript-eslint/type-annotation-spacing": [
			"warn",
			{
				"before": false,
				"after": true,
				"overrides": {
					"arrow": {
						"before": true,
						"after": true
					}
				}
			}
		],
		"@typescript-eslint/unified-signatures": ["warn"],
		// Extension rules!
		"brace-style": "off",
		"@typescript-eslint/brace-style": [
			"error",
			"stroustrup",
			{
				"allowSingleLine": true
			}
		],
		"default-param-last": "off",
		"@typescript-eslint/default-param-last": [
			"error"
		],
		"func-call-spacing": "off",
		"@typescript-eslint/func-call-spacing": [
			"warn",
			"never"
		],
		"indent": "off",
		"@typescript-eslint/indent": [
			"error",
			"tab",
			{
				"FunctionDeclaration": {
					"body": 1,
					"parameters": 1
				}
			}
		],
		"no-array-constructor": "off",
		"@typescript-eslint/no-array-constructor": ["error"],
		"no-extra-semi": "off",
		"@typescript-eslint/no-extra-semi": ["warn"],
		// "no-magic-numbers": "off",
		// "@typescript-eslint/no-magic-numbers": ["error", { "ignoreNumericLiteralTypes": true }],
		"no-unused-expressions": "off",
		"@typescript-eslint/no-unused-expressions": ["error"],
		// "semi": "off",
		// "@typescript-eslint/semi": [
		//     "error"
		//
		// ],
		"space-before-function-paren": "off",
		// want it on for regular functions, off for methods?
		// "@typescript-eslint/space-before-function-paren": [
		// 	"warn",
		// 	{
		// 		"anonymous": "never",
		// 		"named": "always",
		// 		"asyncArrow": "always"
		// 	}
		// ],
		// End extension rules
		"constructor-super": "warn",
		"eol-last": "warn",
		"eqeqeq": [
			"warn",
			"smart"
		],
		"linebreak-style": [
			"warn",
			"unix"
		],
		"no-duplicate-case": "warn",
		"no-duplicate-imports": "warn",
		"no-eval": "warn",
		"no-new-wrappers": "warn",
		"no-redeclare": "warn",
		"no-restricted-syntax": [
			"warn",
			"ForInStatement"
		],
		"no-sequences": "warn",
		"no-var": "warn",
		"prefer-const": "warn",
		"quote-props": [
			"warn",
			"as-needed"
		],
		"radix": "error",
		"use-isnan": "warn",
		"@typescript-eslint/tslint/config": [
			"error",
			{
				"rules": {
					"encoding": true,
					"no-boolean-literal-compare": true,
					"no-tautology-expression": true,
					"static-this": true
				}
			}
		],
		//
		"for-direction": "error",
		// probably will never need a getter than returns `undefined`.
		"getter-return": ["error"],
		"no-compare-neg-zero": "error",
		"no-cond-assign": ["error", "except-parens"],
		"no-dupe-args": "error",
		"no-dupe-else-if": "error",
		"no-dupe-keys": "error",
		"no-duplicate-case": "error",
		"no-empty": "warn",
		"no-empty-character-class": "error",
		"no-ex-assign": "error",
		"no-extra-boolean-cast": "error",
		"no-unsafe-finally": "error",
		"no-obj-calls": "error",
		"no-irregular-whitespace": "error",
		// styling
		"dot-location": ["error", "property"],
		// "dot-notation": [
		// 	"error",
		// 	{
		// 		"allowKeywords": true
		// 	}
		// ],
		"grouped-accessor-pairs": [
			"error",
			"getBeforeSet"
		],
		"no-octal": "error",
		"no-with": "error",
		"wrap-iife": ["error", "inside"],
		"yoda": [
			"warn",
			"never", {
				"exceptRange": true
			}
		],
		// variables
		"no-undef-init": "error",
		"no-shadow-restricted-names": "error",
		"no-label-var": "error",
		"no-use-before-define": "error",
		// nodejs/commonjs
		// stylistic
		"array-bracket-newline": [
			"warn",
			{
				"multiline": true
			}
		],
		"block-spacing": ["warn", "always"],
		"no-lonely-if": "warn",
		"no-mixed-operators": "error",
		"one-var": ["error", "never"],
		"prefer-numeric-literals": "warn",
		"require-yield": "warn",
		"no-useless-computed-key": ["warn"],
		"no-regex-spaces": "warn"
	}
};
