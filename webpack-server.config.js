const path = require('path');

module.exports = {
	entry: './src/ts/server/index.ts',
	mode: 'development',

	target: 'node',
	node: {
		__dirname: false,
		__filename: false
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: [{
					loader: 'ts-loader',
					options: {
						configFile: "tsconfig-server.json"
					}
				}],
				// include: /src\/ts\/client/,
				// exclude: /node_modules/
				exclude: path.resolve(__dirname, 'node_modules')
			}
		]
	},
	resolve: {
		extensions: ['.ts', '.js'],
		alias: {
			'_': path.resolve(__dirname, "src","ts"),
			'_co': path.resolve(__dirname, "src", "ts", "shared", "config.ts"),
			'_c': path.resolve(__dirname, "src", "ts", "client"),
			'_sh': path.resolve(__dirname, "src", "ts", "shared"),
			'_s': path.resolve(__dirname, "src", "ts", "server")
		},
		// modules: [
		// 	path.resolve(__dirname, "node_modules")
		// ]
	},
	// externals: {
	// 	'phaser-ce': 'Phaser',
	// 	'socket.io': 'socketio'
	// },
	externals: [require('webpack-node-externals')()],
	output: {
		filename: 'index.js',
		path: path.resolve(__dirname, 'build', 'server'),
		// publicPath
	}
};
