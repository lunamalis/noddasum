/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/ts/client/client.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/ts/client/classes/Star.ts":
/*!***************************************!*\
  !*** ./src/ts/client/classes/Star.ts ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nconst phaser_ce_1 = __importDefault(__webpack_require__(/*! phaser-ce */ \"phaser-ce\"));\nmodule.exports = class Star {\n    constructor(state, id, point) {\n        this.hyperlanes = [];\n        this.state = state;\n        this.position = point;\n        this.id = id;\n        // this.id = this.state.newStarId();\n        const sprite = this.state.game.add.sprite(point.x, point.y, this.generateTexture());\n        sprite.data.star = this;\n        sprite.anchor.setTo(0.5, 0.5);\n        sprite.inputEnabled = true;\n        sprite.events.onInputDown.add(this.onClick.bind(this));\n        this.sprite = sprite;\n    }\n    generateTexture(color = 0xffffff, diameter = 10) {\n        const graphics = new phaser_ce_1.default.Graphics(this.state.game, 0, 0);\n        graphics.beginFill(color)\n            .drawCircle(0, 0, diameter)\n            .endFill();\n        const returner = graphics.generateTexture();\n        graphics.destroy();\n        return returner;\n    }\n    /**\n     * \"Activate\" this star, making it active.\n     */\n    activate() {\n        this.sprite.loadTexture(this.generateTexture(0x0000ff));\n    }\n    deactivate() {\n        this.sprite.loadTexture(this.generateTexture());\n    }\n    onClick() {\n        this.activate();\n        this.state.starsById[this.state.activeStar].deactivate();\n        this.state.activeStar = this.id;\n    }\n};\n\n\n//# sourceURL=webpack:///./src/ts/client/classes/Star.ts?");

/***/ }),

/***/ "./src/ts/client/classes/battle/Ship.ts":
/*!**********************************************!*\
  !*** ./src/ts/client/classes/battle/Ship.ts ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nconst phaser_ce_1 = __importDefault(__webpack_require__(/*! phaser-ce */ \"phaser-ce\"));\nmodule.exports = class Ship {\n    constructor(battle, id, group, collision, shipType) {\n        this.battle = battle;\n        this.id = id;\n        this.game = battle.game;\n        this.shipType = shipType;\n        this.sprite = group.create(Math.floor(Math.random() * 50) + 50, 50, `ship--${this.shipType.domain}-${this.shipType.name}`);\n        const s = this.sprite;\n        this.game.physics.enable(s, phaser_ce_1.default.Physics.P2JS);\n        const b = s.body;\n        console.log(this.shipType);\n        b.addPolygon({}, this._copy_shape());\n        b.setCollisionGroup(collision);\n        b.collides([collision]);\n        b.angularDamping = 0.1;\n    }\n    _copy_shape() {\n        const returner = [];\n        for (const i of this.shipType.shape) {\n            returner.push(i.slice());\n        }\n        return returner;\n    }\n};\n\n\n//# sourceURL=webpack:///./src/ts/client/classes/battle/Ship.ts?");

/***/ }),

/***/ "./src/ts/client/client.ts":
/*!*********************************!*\
  !*** ./src/ts/client/client.ts ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nObject.defineProperty(exports, \"__esModule\", { value: true });\nconst _co_1 = __importDefault(__webpack_require__(/*! _co */ \"./src/ts/shared/config.ts\"));\nconst client_class_1 = __importDefault(__webpack_require__(/*! _c/client_class */ \"./src/ts/client/client_class.ts\"));\nfunction main() {\n    console.log(\"Welcome to version \" + _co_1.default.VERSION + \".\");\n    window.out = window.out || {};\n    out.main = main;\n    const c = new client_class_1.default();\n    out.c = c;\n    c.start();\n}\nwindow.addEventListener(\"DOMContentLoaded\", main);\n\n\n//# sourceURL=webpack:///./src/ts/client/client.ts?");

/***/ }),

/***/ "./src/ts/client/client_class.ts":
/*!***************************************!*\
  !*** ./src/ts/client/client_class.ts ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nvar _a;\nconst phaser_ce_1 = __importDefault(__webpack_require__(/*! phaser-ce */ \"phaser-ce\"));\nconst title_state_1 = __importDefault(__webpack_require__(/*! ./states/title_state */ \"./src/ts/client/states/title_state.ts\"));\nconst starmap_state_1 = __importDefault(__webpack_require__(/*! ./states/starmap_state */ \"./src/ts/client/states/starmap_state.ts\"));\nconst battle_state_1 = __importDefault(__webpack_require__(/*! ./states/battle_state */ \"./src/ts/client/states/battle_state.ts\"));\nmodule.exports = (_a = class Client {\n        constructor() {\n            if (Client.exists) {\n                throw new Error(\"Client already exists! Only one Client may exist.\");\n            }\n            Client.exists = true;\n        }\n        start() {\n            this.game = new phaser_ce_1.default.Game(800, 400, phaser_ce_1.default.AUTO, 'mainCanvas');\n            // @ts-ignore\n            this.socket = io();\n            this.game.state.add(title_state_1.default.NAME, new title_state_1.default());\n            this.game.state.add(starmap_state_1.default.NAME, new starmap_state_1.default());\n            this.game.state.add(battle_state_1.default.NAME, new battle_state_1.default());\n            console.log(\"States number: \");\n            // for (let i = 0; i < this.game.state.states.length; i++) {\n            for (const [key, value] of Object.entries(this.game.state.states)) {\n                // @ts-ignore\n                value.client = this;\n            }\n            this.game.state.start(title_state_1.default.NAME);\n        }\n    },\n    _a.exists = false,\n    _a);\n\n\n//# sourceURL=webpack:///./src/ts/client/client_class.ts?");

/***/ }),

/***/ "./src/ts/client/states/battle_state.ts":
/*!**********************************************!*\
  !*** ./src/ts/client/states/battle_state.ts ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nvar _a;\nconst phaser_ce_1 = __importDefault(__webpack_require__(/*! phaser-ce */ \"phaser-ce\"));\nconst Ship_1 = __importDefault(__webpack_require__(/*! ../classes/battle/Ship */ \"./src/ts/client/classes/battle/Ship.ts\"));\nconst deepFreeze_1 = __importDefault(__webpack_require__(/*! _/shared/util/deepFreeze */ \"./src/ts/shared/util/deepFreeze.ts\"));\nmodule.exports = (_a = class BattleState extends phaser_ce_1.default.State {\n        constructor() {\n            super(...arguments);\n            this._showDebug = false;\n        }\n        init() {\n            this.ships = [];\n            this.shipTypes = {};\n            this.client.socket.on(\"replyShipTypes\", this._replyShipTypes.bind(this));\n            this.client.socket.on(\"replyBattleInit\", this._onReplyBattleInit.bind(this));\n            this.client.socket.on(\"battleCreate\", this._battleCreate.bind(this));\n        }\n        preload() {\n            this.game.load.image('faction5ship1', 'resources/assets/lunamalis/textures/F5S1.png');\n            this.game.load.image('faction5ship2', 'resources/assets/lunamalis/textures/F5S2.png');\n        }\n        create() {\n            this._keys = {\n                debug: this.game.input.keyboard.addKey(phaser_ce_1.default.Keyboard.UNDERSCORE)\n            };\n            this.game.physics.startSystem(phaser_ce_1.default.Physics.P2JS);\n            this.game.world.setBounds(-1000, -1000, 1000, 1000);\n            this.game.physics.p2.setImpactEvents(true);\n            this.game.physics.p2.restitution = 0.8;\n            this._collision = this.game.physics.p2.createCollisionGroup();\n            this.game.physics.p2.updateBoundsCollisionGroup();\n            this._group = this.game.add.group();\n            this._group.enableBody = true;\n            this._group.physicsBodyType = phaser_ce_1.default.Physics.P2JS;\n            this.game.time.desiredFps = 60;\n            this._cursors = this.game.input.keyboard.createCursorKeys();\n            this._playerShipId = 0;\n            this.client.socket.emit(\"battleInit\");\n        }\n        _create_post() {\n            this.game.camera.follow(this.ships[this._playerShipId].sprite);\n        }\n        // private _add_ship(data: BattleShip) {\n        // \tconst st = this._get_ship_type_by_did(data.shipDomain, data.shipId);\n        // \tconst ship = new Ship(this, data.id, this._group, this._collision, st.name);\n        // \tthis.ships.push(ship);\n        // }\n        _add_ship(ship) {\n            const shipType = this._get_ship_type_by_did(ship.domain, ship.name);\n            const s = new Ship_1.default(this, ship.id, this._group, this._collision, shipType);\n            this.ships.push(s);\n        }\n        // did === domain and id\n        _get_ship_type_by_did(domain, id) {\n            const d = this.shipTypes[domain];\n            if (d === undefined) {\n                throw new Error(`Undefined ship domain: ${domain}. Also requested id ${id}.`);\n            }\n            // See?\n            const st = d[id];\n            if (st === undefined) {\n                throw new Error(`Undefined ship id: ${id}. Also requested domain ${domain}.`);\n            }\n            return st;\n        }\n        /**\n         * This is called when the server replies with the \"replyBattleInit\" message.\n         *\n         * It brings us the required ship types in the battle.\n         *\n         * @param battleInit\n         */\n        _onReplyBattleInit(battleInit) {\n            const needed = {};\n            for (const [domain, ids] of Object.entries(battleInit)) {\n                if (this.shipTypes[domain] === undefined) {\n                    this.shipTypes[domain] = {};\n                    needed[domain] = [];\n                    // If we don't have anything in this domain, just add all of the ids into our `needed` array.\n                    for (const id of ids) {\n                        needed[domain].push(id);\n                    }\n                }\n                for (const id of ids) {\n                    if (this.shipTypes[domain][id] === undefined) {\n                        needed[domain].push(id);\n                    }\n                }\n            }\n            this.client.socket.emit(\"understandShipTypes\", needed);\n        }\n        _set_ship_type_by_did(domain, id, st) {\n            this.shipTypes[domain] || (this.shipTypes[domain] = {});\n            this.shipTypes[domain][id] = st;\n        }\n        _replyShipTypes(data) {\n            console.log(\"Ship types:\", data);\n            for (const [domain, ids] of Object.entries(data)) {\n                (this.shipTypes[domain] === undefined) && (this.shipTypes[domain] = {});\n                for (const [name, value] of Object.entries(ids)) {\n                    // Freeze!\n                    this.shipTypes[domain][name] = deepFreeze_1.default(value);\n                    // Also laod our assets.\n                    const textureName = `ship--${domain}-${name}`;\n                    const texturePath = `resources/assets/${domain}/textures/${value.texture}.png`;\n                    console.log(\"Loading texture:\", textureName, texturePath);\n                    this.game.load.image(textureName, texturePath);\n                }\n            }\n            this.game.load.start();\n            this.game.load.onLoadComplete.addOnce(() => {\n                this.client.socket.emit(\"battleReady\");\n            });\n        }\n        _battleCreate(data) {\n            console.log(\"Adding ships:\", data);\n            for (const ship of data) {\n                this._add_ship(ship);\n            }\n            this._create_post();\n        }\n        update() {\n            const p = this.ships[this._playerShipId];\n            if (p) {\n                if (this._cursors.up.isDown) {\n                    p.sprite.body.thrust(200);\n                }\n                else if (this._cursors.down.isDown) {\n                    p.sprite.body.thrust(-200);\n                }\n                if (this._cursors.left.isDown) {\n                    p.sprite.body.angularForce -= 1;\n                }\n                else if (this._cursors.right.isDown) {\n                    p.sprite.body.angularForce += 1;\n                }\n            }\n            if (this._keys.debug.justDown) {\n                console.log(`Setting debug mode to ${!this._showDebug}`);\n                this._showDebug = !this._showDebug;\n                for (const ship of Object.values(this.ships)) {\n                    // this.game.debug.body(ship.sprite);\n                    ship.sprite.body.debug = this._showDebug;\n                }\n                if (!this._showDebug) {\n                    this.game.debug.reset();\n                }\n            }\n        }\n        render() {\n            if (this._showDebug) {\n            }\n        }\n    },\n    _a.NAME = \"Battle\",\n    _a);\n\n\n//# sourceURL=webpack:///./src/ts/client/states/battle_state.ts?");

/***/ }),

/***/ "./src/ts/client/states/starmap_state.ts":
/*!***********************************************!*\
  !*** ./src/ts/client/states/starmap_state.ts ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nvar _a;\nconst phaser_ce_1 = __importDefault(__webpack_require__(/*! phaser-ce */ \"phaser-ce\"));\nconst Star_1 = __importDefault(__webpack_require__(/*! _c/classes/Star */ \"./src/ts/client/classes/Star.ts\"));\nconst battle_state_1 = __importDefault(__webpack_require__(/*! ./battle_state */ \"./src/ts/client/states/battle_state.ts\"));\nmodule.exports = (_a = class StarmapState extends phaser_ce_1.default.State {\n        init() {\n            this.stars = [];\n            this.starsById = {};\n            this._starIds = [];\n            this.hyperlaneGraphics = [];\n        }\n        // preload(): void {}\n        create() {\n            let goText = this.game.add.text(10, 10, 'Go', {\n                font: '1em Courier',\n                fill: '#ffffff',\n                align: 'center'\n            });\n            goText.inputEnabled = true;\n            goText.events.onInputDown.add(this.onClickGo.bind(this));\n            // this._generateStars();\n            // this.activeStar = this.getRandomStarId();\n            // this._connectStars();\n            this.client.socket.on('sendMap', this.onGetMap.bind(this));\n            this.client.socket.emit('requestMap');\n        }\n        onGetMap(message) {\n            console.log(message);\n            for (let starIndex in message.stars) {\n                let star = message.stars[starIndex];\n                console.log(star);\n                this._buildStar(star);\n            }\n        }\n        _buildStar(star) {\n            let s = new Star_1.default(this, star.id, new phaser_ce_1.default.Point(star.x, star.y));\n            this.starsById[star.id] = s;\n            this.stars.push(s);\n            return s;\n        }\n        static getRandomXY() {\n            return new phaser_ce_1.default.Point(Math.floor(Math.random() * 250), Math.floor(Math.random() * 250));\n        }\n        getRandomStarId() {\n            return this._starIds[Math.floor(Math.random() * this._starIds.length)];\n        }\n        getRandomStar() {\n            return this.starsById[this.getRandomStarId()];\n        }\n        getTwoStars() {\n            let a = this.getRandomStar();\n            let tries = 0;\n            while (tries < 100) {\n                let try_ = this.getRandomStar();\n                if (try_.id !== a.id) {\n                    return [a, try_];\n                }\n            }\n            throw new Error(\"Tried over 100 times to get two random stars.\");\n        }\n        onClickGo() {\n            this.game.state.start(battle_state_1.default.NAME);\n        }\n    },\n    _a.NAME = \"Starmap\",\n    _a);\n\n\n//# sourceURL=webpack:///./src/ts/client/states/starmap_state.ts?");

/***/ }),

/***/ "./src/ts/client/states/title_state.ts":
/*!*********************************************!*\
  !*** ./src/ts/client/states/title_state.ts ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nvar _a;\nconst phaser_ce_1 = __importDefault(__webpack_require__(/*! phaser-ce */ \"phaser-ce\"));\nconst starmap_state_1 = __importDefault(__webpack_require__(/*! ./starmap_state */ \"./src/ts/client/states/starmap_state.ts\"));\nmodule.exports = (_a = class TitleState extends phaser_ce_1.default.State {\n        init() {\n            this.texts = {\n                title: null,\n                play: null\n            };\n            this.titleTextColorPosition = 0;\n        }\n        preload() {\n        }\n        create() {\n            console.log(\"Hello, title.\");\n            this.game.stage.backgroundColor = '#000000';\n            this.texts.title = this.game.add.text(this.game.world.width / 2, this.game.world.height / 2, \"Lunamalis\", {\n                font: \"2em Arial\",\n                fill: \"#eeeeee\",\n                align: \"center\"\n            });\n            this.texts.title.anchor.setTo(0.5, 0.5);\n            this.texts.title.addColor(TitleState.DefaultTitleColor, 0);\n            this.texts.play = this.game.add.text(this.game.world.width / 2, this.game.world.height / 2 + 50, \"Play\", {\n                font: \"1em Arial\",\n                fill: \"#ffffff\",\n                align: \"center\"\n            });\n            this.texts.play.inputEnabled = true;\n            this.texts.play.events.onInputOver.add(this.play.bind(this));\n        }\n        update() {\n            if (!this.titleTimeout) {\n                for (let i = 0; i < TitleState.titleTextColors.length; i++) {\n                    const index = TitleState.wrapInt(i - this.titleTextColorPosition, TitleState.titleTextColors.length);\n                    const color = TitleState.titleTextColors[index];\n                    this.texts.title.addColor(color, i);\n                }\n                this.titleTextColorPosition += 1;\n                this.titleTimeout = setTimeout(this.resetTimeout.bind(this), TitleState.titleSpeed);\n            }\n        }\n        play() {\n            this.game.state.start(starmap_state_1.default.NAME);\n        }\n        resetTimeout() {\n            this.titleTimeout = null;\n        }\n        static wrapInt(value, maximum) {\n            if (value >= 0) {\n                return value % maximum;\n            }\n            return (value % maximum + maximum) % maximum;\n        }\n    },\n    _a.NAME = \"title\",\n    // Lunamalis - 9 letters, 9 slots\n    _a.titleTextColors = [\n        '#333333',\n        '#444444',\n        '#333333',\n        '#444444',\n        '#555555',\n        '#777777',\n        '#888888',\n        '#999999',\n        '#aaaaaa'\n    ],\n    _a.DefaultTitleColor = '#eeeeee',\n    _a.titleSpeed = 50,\n    _a);\n\n\n//# sourceURL=webpack:///./src/ts/client/states/title_state.ts?");

/***/ }),

/***/ "./src/ts/shared/config.ts":
/*!*********************************!*\
  !*** ./src/ts/shared/config.ts ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nconst data = {\n    // Make sure that this is equivalent to `version` in `package.json`.\n    VERSION: \"0.0.1\",\n    PORT: 8090\n};\nmodule.exports = Object.freeze(data);\n\n\n//# sourceURL=webpack:///./src/ts/shared/config.ts?");

/***/ }),

/***/ "./src/ts/shared/util/deepFreeze.ts":
/*!******************************************!*\
  !*** ./src/ts/shared/util/deepFreeze.ts ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nmodule.exports = function deepFreeze(obj) {\n    Object.freeze(obj);\n    for (const property of Object.getOwnPropertyNames(obj)) {\n        const t = typeof obj[property];\n        if (t === \"object\" || t === \"function\") {\n            deepFreeze(obj[property]);\n        }\n    }\n    return obj;\n};\n\n\n//# sourceURL=webpack:///./src/ts/shared/util/deepFreeze.ts?");

/***/ }),

/***/ "phaser-ce":
/*!*************************!*\
  !*** external "Phaser" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = Phaser;\n\n//# sourceURL=webpack:///external_%22Phaser%22?");

/***/ })

/******/ });