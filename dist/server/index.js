/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/ts/server/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/webpack/buildin/module.js":
/*!***********************************!*\
  !*** (webpack)/buildin/module.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = function(module) {\n\tif (!module.webpackPolyfill) {\n\t\tmodule.deprecate = function() {};\n\t\tmodule.paths = [];\n\t\t// module.parent = undefined by default\n\t\tif (!module.children) module.children = [];\n\t\tObject.defineProperty(module, \"loaded\", {\n\t\t\tenumerable: true,\n\t\t\tget: function() {\n\t\t\t\treturn module.l;\n\t\t\t}\n\t\t});\n\t\tObject.defineProperty(module, \"id\", {\n\t\t\tenumerable: true,\n\t\t\tget: function() {\n\t\t\t\treturn module.i;\n\t\t\t}\n\t\t});\n\t\tmodule.webpackPolyfill = 1;\n\t}\n\treturn module;\n};\n\n\n//# sourceURL=webpack:///(webpack)/buildin/module.js?");

/***/ }),

/***/ "./src/ts/server/Server.ts":
/*!*********************************!*\
  !*** ./src/ts/server/Server.ts ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nconst path_1 = __importDefault(__webpack_require__(/*! path */ \"path\"));\nconst express_1 = __importDefault(__webpack_require__(/*! express */ \"express\"));\nconst socket_io_1 = __importDefault(__webpack_require__(/*! socket.io */ \"socket.io\"));\nconst _co_1 = __importDefault(__webpack_require__(/*! _co */ \"./src/ts/shared/config.ts\"));\nconst page_manager_1 = __importDefault(__webpack_require__(/*! _/server/managers/page_manager */ \"./src/ts/server/managers/page_manager.ts\"));\nconst api_1 = __importDefault(__webpack_require__(/*! _/server/pages/api */ \"./src/ts/server/pages/api.ts\"));\nconst http_1 = __importDefault(__webpack_require__(/*! http */ \"http\"));\nconst mod_manager_1 = __importDefault(__webpack_require__(/*! ./managers/mod_manager */ \"./src/ts/server/managers/mod_manager.ts\"));\nconst registry_manager_1 = __importDefault(__webpack_require__(/*! ./managers/registry_manager */ \"./src/ts/server/managers/registry_manager.ts\"));\nmodule.exports = class Server {\n    constructor() {\n        this.app = express_1.default();\n        this.clients = {};\n        this.ABSOLUTE_DIR = __dirname;\n        this.ROOT_DIR = path_1.default.dirname(this.ABSOLUTE_DIR);\n        this.REPO_DIR = path_1.default.dirname(this.ROOT_DIR);\n        this.PUBLIC_DIR = path_1.default.join(this.ROOT_DIR, \"public\");\n        this.NODE_MODULES_DIR = path_1.default.join(this.REPO_DIR, \"node_modules\");\n        this.RESOURCES_DIR = path_1.default.join(this.REPO_DIR, \"resources\");\n        this.MOD_DIR = path_1.default.join(this.ROOT_DIR, \"shared\", \"mods\");\n        this.http = http_1.default.createServer(this.app);\n        this.io = socket_io_1.default(this.http);\n        this.registry_manager = new registry_manager_1.default(this);\n        this.mod_manager = new mod_manager_1.default(this);\n        this.mod_manager.get_mods();\n        this.mod_manager.load_all();\n        this.registry_manager.freeze();\n        this.page_manager = new page_manager_1.default(this);\n        this.page_manager.add_page(new api_1.default());\n        this.page_manager.finish();\n        console.log(\"Public path is at:\", this.PUBLIC_DIR);\n        this.app.use(express_1.default.static(this.PUBLIC_DIR));\n        this.app.use('/resources/', express_1.default.static(this.RESOURCES_DIR));\n        this.app.use('/lib/phaser.js', express_1.default.static(path_1.default.join(this.NODE_MODULES_DIR, 'phaser-ce', 'build', 'phaser.js')));\n        this.app.use('/client.js', express_1.default.static(path_1.default.join(this.ROOT_DIR, 'client', 'main.js')));\n        this.io.on('connection', this.onConnection.bind(this));\n        this.io.on('disconnect', this.onDisconnect.bind(this));\n    }\n    start() {\n        // this.app.listen(CONFIG.PORT);\n        this.http.listen(_co_1.default.PORT);\n    }\n    onConnection(socket) {\n        console.log(\"Connected\", socket.id, \".\");\n        this.clients[socket.id] = socket;\n        socket.on('message', function (message) {\n            console.log('message: ', message);\n        });\n        socket.on('requestMap', this._onRequestMap.bind(this, socket));\n        socket.on('battleInit', this._onBattleInit.bind(this, socket));\n        socket.on('understandShipTypes', this._onUnderstandShipTypes.bind(this, socket));\n        socket.on('battleReady', this._onBattleReady.bind(this, socket));\n    }\n    onDisconnect(socket) {\n        console.log(\"Goodbye socket\", socket.id, \".\");\n        delete this.clients[socket.id];\n    }\n    _onRequestMap(socket) {\n        console.log(\"Requested map.\");\n        const data = {\n            stars: {\n                0: {\n                    id: 0,\n                    x: 100,\n                    y: 100\n                }\n            },\n            position: 0\n        };\n        socket.emit(\"sendMap\", data);\n    }\n    /**\n     * The \"Battle init\" message is used to create the battle.\n     * Ships\n     * @param socket\n     */\n    _onBattleInit(socket) {\n        console.log(\"Requested battle initialization.\");\n        socket.emit(\"replyBattleInit\", {\n            lunamalis: [\n                \"genericShip1\",\n                \"genericShip2\"\n            ]\n        });\n    }\n    _onUnderstandShipTypes(socket, needed) {\n        const sender = {};\n        for (const [domain, ids] of Object.entries(needed)) {\n            // sender.push(this._shipTypeToClient(this.registry_manager.data.ship[shipType]));\n            sender[domain] = {};\n            for (const id of ids) {\n                sender[domain][id] = this._shipTypeToClient(this.registry_manager.data.ship[domain][id]);\n                console.log(\"Doing id, got\", sender[domain][id]);\n            }\n        }\n        socket.emit(\"replyShipTypes\", sender);\n    }\n    _shipTypeToClient(shipType) {\n        return {\n            domain: shipType.domain,\n            name: shipType.name,\n            shape: shipType.shape,\n            texture: shipType.texture\n        };\n    }\n    _onBattleReady(socket) {\n        // Give them a bunch of ships.\n        socket.emit('battleCreate', [\n            {\n                id: 0,\n                domain: \"lunamalis\",\n                name: \"genericShip1\",\n                x: 10,\n                y: 10\n            },\n            {\n                id: 1,\n                domain: \"lunamalis\",\n                name: \"genericShip1\",\n                x: 20,\n                y: 50\n            },\n            {\n                id: 2,\n                domain: \"lunamalis\",\n                name: \"genericShip2\",\n                x: 30,\n                y: 100\n            }\n        ]);\n    }\n};\n\n\n//# sourceURL=webpack:///./src/ts/server/Server.ts?");

/***/ }),

/***/ "./src/ts/server/base_manager.ts":
/*!***************************************!*\
  !*** ./src/ts/server/base_manager.ts ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nmodule.exports = class BaseManager {\n    constructor(server) {\n        this.server = server;\n    }\n};\n\n\n//# sourceURL=webpack:///./src/ts/server/base_manager.ts?");

/***/ }),

/***/ "./src/ts/server/base_page.ts":
/*!************************************!*\
  !*** ./src/ts/server/base_page.ts ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nclass BasePage {\n}\nmodule.exports = BasePage;\n\n\n//# sourceURL=webpack:///./src/ts/server/base_page.ts?");

/***/ }),

/***/ "./src/ts/server/index.ts":
/*!********************************!*\
  !*** ./src/ts/server/index.ts ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("/* WEBPACK VAR INJECTION */(function(module) {\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nObject.defineProperty(exports, \"__esModule\", { value: true });\n// Entry file.\nconst Server_1 = __importDefault(__webpack_require__(/*! _/server/Server */ \"./src/ts/server/Server.ts\"));\nfunction main() {\n    const s = new Server_1.default();\n    s.start();\n}\nif (__webpack_require__.c[__webpack_require__.s] === module) {\n    main();\n}\n\n/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack/buildin/module.js */ \"./node_modules/webpack/buildin/module.js\")(module)))\n\n//# sourceURL=webpack:///./src/ts/server/index.ts?");

/***/ }),

/***/ "./src/ts/server/managers/mod_manager.ts":
/*!***********************************************!*\
  !*** ./src/ts/server/managers/mod_manager.ts ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nmodule.exports = class ModManager {\n    constructor(server) {\n        this.server = server;\n        this.MOD_DIR = this.server.MOD_DIR;\n        this.registry = this.server.registry_manager;\n        // this.mods = mod_list;\n        this.mods = {};\n    }\n    get_mods() {\n        const mods_list = __webpack_require__(/*! _sh/mods/mod_list */ \"./src/ts/shared/mods/mod_list.ts\");\n        for (const [mod_key, mod_value] of Object.entries(mods_list)) {\n            // @ts-ignore\n            this.mods[mod_value.NAME] = new mod_value(this);\n        }\n        console.log(`Loaded ${this.length} mods.`);\n        console.log(\"Mods:\", this.mods);\n    }\n    load_all() {\n        for (const mod of Object.values(this.mods)) {\n            mod.build();\n        }\n        for (const mod of Object.values(this.mods)) {\n            mod.preInit();\n        }\n        for (const mod of Object.values(this.mods)) {\n            mod.init();\n        }\n        for (const mod of Object.values(this.mods)) {\n            mod.postInit();\n        }\n    }\n    get length() {\n        return Object.keys(this.mods).length - 1;\n    }\n    registerShip(obj) {\n        this.registry.addShip(obj);\n    }\n};\n\n\n//# sourceURL=webpack:///./src/ts/server/managers/mod_manager.ts?");

/***/ }),

/***/ "./src/ts/server/managers/page_manager.ts":
/*!************************************************!*\
  !*** ./src/ts/server/managers/page_manager.ts ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nconst base_manager_1 = __importDefault(__webpack_require__(/*! _/server/base_manager */ \"./src/ts/server/base_manager.ts\"));\nmodule.exports = class PageManager extends base_manager_1.default {\n    constructor(server) {\n        super(server);\n        this.routes = {};\n    }\n    add_page(page) {\n        console.log(\"Adding new page at \" + page.route + \".\");\n        this.routes[page.route] = page;\n    }\n    finish() {\n        for (const [route, page] of Object.entries(this.routes)) {\n            console.log(\"Setting up \" + route + \".\");\n            page.setup(this.server.app);\n        }\n    }\n};\n\n\n//# sourceURL=webpack:///./src/ts/server/managers/page_manager.ts?");

/***/ }),

/***/ "./src/ts/server/managers/registry_manager.ts":
/*!****************************************************!*\
  !*** ./src/ts/server/managers/registry_manager.ts ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nmodule.exports = class RegistryManager {\n    constructor(server) {\n        this.server = server;\n        this.data = {\n            ship: {}\n        };\n    }\n    freeze() {\n        Object.freeze(this.data);\n    }\n    addShip(obj) {\n        (this.data.ship[obj.domain] === undefined) && (this.data.ship[obj.domain] = {});\n        this.data.ship[obj.domain][obj.name] = obj;\n    }\n};\n\n\n//# sourceURL=webpack:///./src/ts/server/managers/registry_manager.ts?");

/***/ }),

/***/ "./src/ts/server/pages/api.ts":
/*!************************************!*\
  !*** ./src/ts/server/pages/api.ts ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nconst base_page_1 = __importDefault(__webpack_require__(/*! _/server/base_page */ \"./src/ts/server/base_page.ts\"));\nmodule.exports = class APIPage extends base_page_1.default {\n    constructor() {\n        super(...arguments);\n        this.route = '/api';\n    }\n    setup(app) {\n        app.get(this.route, this.get.bind(this));\n    }\n    get(req, res) {\n        res.send(\"Greetings.\");\n    }\n};\n\n\n//# sourceURL=webpack:///./src/ts/server/pages/api.ts?");

/***/ }),

/***/ "./src/ts/shared/config.ts":
/*!*********************************!*\
  !*** ./src/ts/shared/config.ts ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nconst data = {\n    // Make sure that this is equivalent to `version` in `package.json`.\n    VERSION: \"0.0.1\",\n    PORT: 8090\n};\nmodule.exports = Object.freeze(data);\n\n\n//# sourceURL=webpack:///./src/ts/shared/config.ts?");

/***/ }),

/***/ "./src/ts/shared/interfaces/Mod.ts":
/*!*****************************************!*\
  !*** ./src/ts/shared/interfaces/Mod.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nclass Mod {\n    constructor(mana) {\n        this.mana = mana;\n    }\n    /**\n     * Here, set up extra stuff, before mod loading.\n     * Do *not* do things like read configurations here.\n     * Do that later.\n     */\n    build() { }\n    registerShip(obj) {\n        this.mana.registry.addShip({\n            domain: this.constructor.NAME,\n            name: obj.name,\n            shape: obj.shape,\n            texture: obj.texture\n        });\n    }\n}\nmodule.exports = Mod;\n\n\n//# sourceURL=webpack:///./src/ts/shared/interfaces/Mod.ts?");

/***/ }),

/***/ "./src/ts/shared/mods sync recursive ^\\.\\/.*\\/__init__$":
/*!****************************************************!*\
  !*** ./src/ts/shared/mods sync ^\.\/.*\/__init__$ ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var map = {\n\t\"./lunamalis/__init__\": \"./src/ts/shared/mods/lunamalis/__init__.ts\"\n};\n\n\nfunction webpackContext(req) {\n\tvar id = webpackContextResolve(req);\n\treturn __webpack_require__(id);\n}\nfunction webpackContextResolve(req) {\n\tif(!__webpack_require__.o(map, req)) {\n\t\tvar e = new Error(\"Cannot find module '\" + req + \"'\");\n\t\te.code = 'MODULE_NOT_FOUND';\n\t\tthrow e;\n\t}\n\treturn map[req];\n}\nwebpackContext.keys = function webpackContextKeys() {\n\treturn Object.keys(map);\n};\nwebpackContext.resolve = webpackContextResolve;\nmodule.exports = webpackContext;\nwebpackContext.id = \"./src/ts/shared/mods sync recursive ^\\\\.\\\\/.*\\\\/__init__$\";\n\n//# sourceURL=webpack:///./src/ts/shared/mods_sync_^\\.\\/.*\\/__init__$?");

/***/ }),

/***/ "./src/ts/shared/mods/lunamalis/__init__.ts":
/*!**************************************************!*\
  !*** ./src/ts/shared/mods/lunamalis/__init__.ts ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nvar _a;\nconst Mod_1 = __importDefault(__webpack_require__(/*! _/shared/interfaces/Mod */ \"./src/ts/shared/interfaces/Mod.ts\"));\nmodule.exports = (_a = class Lunamalis extends Mod_1.default {\n        preInit() { }\n        init() {\n            this.registerShip({\n                name: \"genericShip1\",\n                shape: [\n                    [0, 0],\n                    [200, 0],\n                    [200, 200],\n                    [0, 200]\n                ],\n                texture: \"F5S1\"\n            });\n            this.registerShip({\n                name: \"genericShip2\",\n                shape: [\n                    [0, 0],\n                    [50, 10],\n                    [100, 0],\n                    [100, 100],\n                    [0, 100]\n                ],\n                texture: \"F5S2\"\n            });\n        }\n        postInit() { }\n    },\n    _a.NAME = \"lunamalis\",\n    _a.VERSION = \"0.0.1\",\n    _a.VERSION_NUMBER = 0,\n    _a.FRIENDLY_NAME = \"Lunamalis\",\n    _a.DESCRIPTION = \"The base game for Noddasum.\",\n    _a.NODDASUM_VERSION = [\"*\"],\n    _a.AUTHOR = [\"Clothlen\"],\n    _a.CREDITS = \"You-know-who.\",\n    _a);\n\n\n//# sourceURL=webpack:///./src/ts/shared/mods/lunamalis/__init__.ts?");

/***/ }),

/***/ "./src/ts/shared/mods/mod_list.ts":
/*!****************************************!*\
  !*** ./src/ts/shared/mods/mod_list.ts ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n// Add your mods here.\nconst mods = [\n    \"lunamalis\"\n];\n// Don't need to touch anything below here.\nconst mods_out = {};\nfor (const mod of mods) {\n    mods_out[mod] = __webpack_require__(\"./src/ts/shared/mods sync recursive ^\\\\.\\\\/.*\\\\/__init__$\")(`./${mod}/__init__`);\n    // Now let's check that mod.\n    const m = mods_out[mod];\n    for (const item of [\n        \"NAME\",\n        \"VERSION\",\n        \"VERSION_NUMBER\",\n        \"FRIENDLY_NAME\",\n        \"DESCRIPTION\",\n        \"NODDASUM_VERSION\",\n        \"AUTHOR\",\n        \"CREDITS\"\n    ]) {\n        if (m[item] === undefined) {\n            throw new Error(`Invalid Mod class. Item ${item} was not found on class ${m}.`);\n        }\n    }\n}\nmodule.exports = mods_out;\n\n\n//# sourceURL=webpack:///./src/ts/shared/mods/mod_list.ts?");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express\");\n\n//# sourceURL=webpack:///external_%22express%22?");

/***/ }),

/***/ "http":
/*!***********************!*\
  !*** external "http" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"http\");\n\n//# sourceURL=webpack:///external_%22http%22?");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"path\");\n\n//# sourceURL=webpack:///external_%22path%22?");

/***/ }),

/***/ "socket.io":
/*!****************************!*\
  !*** external "socket.io" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"socket.io\");\n\n//# sourceURL=webpack:///external_%22socket.io%22?");

/***/ })

/******/ });