#!/usr/bin/env bash
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

npx webpack --config "$SCRIPTPATH/../webpack-server.config.js" --watch
