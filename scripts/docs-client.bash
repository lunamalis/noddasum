#!/usr/bin/env bash
set -eu
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
"$SCRIPTPATH/../node_modules/.bin/typedoc" --tsconfig "$SCRIPTPATH/../tsconfig-client.json" --theme "$SCRIPTPATH/../node_modules/typedoc-neo-theme/bin/default"
