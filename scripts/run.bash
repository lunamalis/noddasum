#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -eu
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

node "$SCRIPTPATH/../build/server/index.js" "$@"
