#!/usr/bin/env bash
set -eu
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
[ -e "$SCRIPTPATH/../build/public" ] && rm -r "$SCRIPTPATH/../build/public"


cp -r "$SCRIPTPATH/../src/public/" "$SCRIPTPATH/../build/public/"
cp -r "$SCRIPTPATH/../src/public/html/." "$SCRIPTPATH/../build/public/"