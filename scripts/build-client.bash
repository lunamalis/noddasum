#!/usr/bin/env bash
set -eu
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
npx webpack --config "$SCRIPTPATH/../webpack-client.config.js" --watch
