#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# Run proper building first *before* running this script.
set -eux
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

[ -d "$SCRIPTPATH/../dist" ] || mkdir "$SCRIPTPATH/../dist"
[ -d "$SCRIPTPATH/../docs/dist" ] || mkdir "$SCRIPTPATH/../docs/dist"

cp -r "$SCRIPTPATH/../build/." "$SCRIPTPATH/../dist"
cp -r "$SCRIPTPATH/../docs/build/." "$SCRIPTPATH/../docs/dist"

echo 'Update package.json>version. Then commit.'

echo 'Run `git tag -a v{version} -m "version info"`'




#
