# Battle protocol

This document details how the network protocol for battles work in Noddasum.

1. The client sends a "battle init" message to the server.
2. The server replies back with the ship types that will be used in the battle.
3. The client then requests those ship types, pulling from cache if needed.
4. The server replies with details on those ship types. If supported, the client should place these into a cache.
5. The client responds with an "battle ready" message, to let the server know it is ready for the battle.
6. The server replies with a "battle create" message, holding details about the ships in the battle, positions, etc.
7. An infinite loop begins.
7.1. The client sends "battle instruction" messages, detailing how the player interacts with the world.
7.2. The server sends a "battle update" message, detailing the new positions, health, etc., of the everything in the world.
7.3. When the client's player wants to end, a "battle player-instructed end" message is sent to the server. The server ends the battle.
8. The client is sent back to the starmap scene.

The rest is simple enough.
